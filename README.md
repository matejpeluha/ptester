
Medzi základné a najdôležitejšie vyučovacie predmety na STU FEI v odbore aplikovaná informatika, je Programovanie 2. Študenti sa na tomto predmete zoznamujú s programovacím jazykom C aj cez rôzne domáce zadanie. Vzhľadom na vysoký objem zadaní ktoré je potrebné kontrolovať niekedy aj každý týždeň, Ing. Pavlovi Marákovi PhD., cvičiacemu predmetu, by pomohol systém ktorý dokáže tieto domáce zadania otestovať a ohodnotiť. Tento systém, PTester, je teoreticky využiteľný aj na iných predmetoch, kde zadania budú spĺňať formát vhodný pre testovanie v systéme.   

Systém PTester je vytvorený pomocou programovacieho jazyka C++ a frameworku Qt. Vďaka systému je užívateľovi umožnené načítať domáce úlohy ktoré plánuje testovať, v podobe zdrojových kódov študentov napísaných v programovacom jazyku C alebo C++.  
Dôležitou výzvou tejto práce bolo taktiež dostatočné zrýchlenie testovania úloh. K tomuto sme využili princípy multithreadingu, s čím nám pomohol aj framework Qt a jeho knižnice.

PTester popri testovaní zadaní vytvára HTML reporty v ktorých sú rozpísané errory, warningy, výsledky testov aj bodové hodnotenie. Užívateľ si je schopný uložiť všetky výsledky do CSV súboru vo formáte vhodnom pre ais na zapísanie bodov. V systéme je taktiež zabudované podrobné vytváranie testovacích scenárov a testov, na základe ktorých majú byť zvolené študentské zadania ohodnotené.  

Úvodné okno  
![](bp-screenshots/start-window.png)  


Okno na vytváranie testovacích scenárov  
![](bp-screenshots/ptest-creator.png)  


Okno na vytváranie testov  
![](bp-screenshots/test-creator.png)  


Súbor s testovacími scenármi a testami  
![](bp-screenshots/ptest.png)  


Hlavné okno s načítanými zadaniami  
![](bp-screenshots/main-window.png)  


Okno so neplatnými súbormi  
![](bp-screenshots/wrong-submissions.png)  


Hlavné okno počas testovania  
![](bp-screenshots/testing.png)  


Okno pre nastavenia pri ukladaní výsledkov testov  
![](bp-screenshots/settings.png)  


Súbor s uloženými výsledkami testovania  
![](bp-screenshots/csv-file.png)  


Hlavička HTML reportu neskompilovného zadania  
![](bp-screenshots/html-fail.png)  


Hlavička HTML reportu skompilovného zadania  
![](bp-screenshots/html-warning.png)  


Neúspešný test v HTML reporte  
![](bp-screenshots/test-fail.png)  


Úspešný test v HTML reporte  
![](bp-screenshots/test-pass.png)  

