#include "mainwindow.h"
#include "resultsaver.h"
#include "constantholder.h"

#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QDesktopServices>
#include <QStyle>
#include <QCloseEvent>
#include <QDebug>
#include <QInputDialog>


MainWindow::MainWindow(QWidget *parent, QString* homeworksFolder) : QMainWindow(parent), ui(new Ui::MainWindow){
    this->ui->setupUi(this);
    this->ui->searchBox->setText("");
    setWindowTitle("PTester");

    initializeSideAttributes();
    initializeTestingManager();

    initializeHomeWorksTable();
    loadDirectoriesFromHomeworksFolder(*homeworksFolder);

    initializeDialogs();
}

MainWindow::~MainWindow()
{
    delete this->ui;

    delete this->wrongFormatedFilesDialog;
    delete this->reportDialog;

    this->testingManager->stopAllThreads();
    delete this->testingManager;

    delete this->saveSettingsDialog;
}


//inicializacia vedlajsich pomocnych atributov triedy
void MainWindow::initializeSideAttributes(){
    this->numberOfTableRows = 0;
    this->pTestPath = "";
    this->testingRunning = false;
    this->savingRunning = false;
}


//inicializacia dialog-ovych okien
void MainWindow::initializeDialogs(){
    this->reportDialog = new ReportDialog();
    this->wrongFormatedFilesDialog = new WrongSubmissionsDialog();

    this->saveSettingsDialog = new SaveSettingsDialog(this);
    this->saveSettingsDialog->setFolder(this->homeworksFolder);
    connect(this->saveSettingsDialog, SIGNAL(saveInputsAccepted()), this, SLOT(saveResults()));
}


//inicializacia testovacich prvkov
void MainWindow::initializeTestingManager(){
    this->ui->progressBar->hide();
    this->ui->stopTestingButton->hide();

    this->testingManager = new PTestingManager(this->ui->homeWorksTable);
    this->testingManager->setProgressBar(this->ui->progressBar);

    connect(this->testingManager, SIGNAL(testingFinished(void)), this, SLOT(handleTestingFinished(void)));
}


//funkcia sa zavola ked testingManager vy-emituje signal testingFinished, ked je testovanie dokoncene
//v MainWindow vykona potrebne zmeny po ukonceni testovania a informuje o tom uzivatela
void MainWindow::handleTestingFinished(){
    this->testingRunning = false;
    this->ui->homeWorksTable->setSortingEnabled(true);
    this->ui->stopTestingButton->hide();

    QMessageBox::information(this, "PTester", "Testing is finished!");
}


//inicializacia tabulky domacich uloh
void MainWindow::initializeHomeWorksTable(){
    setSectionSizes();
}


//front-end upravy tabulky domacich uloh , povacsine responzivny dizajn
void MainWindow::setSectionSizes(){
    QHeaderView *headerView = this->ui->homeWorksTable->horizontalHeader();

    headerView->setVisible(true);
    headerView->setFrameStyle(QFrame::Box | QFrame::Plain);
    headerView->setLineWidth(1);

    headerView->setSectionResizeMode(QHeaderView::Fixed);
    headerView->setSectionResizeMode(ConstantHolder::NAME_COLUM_INDEX ,QHeaderView::Stretch);
    headerView->setSectionResizeMode(ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX ,QHeaderView::Stretch);
    headerView->resizeSection(ConstantHolder::ID_COLUMN_INDEX, 150);

    for(int index = ConstantHolder::POINTS_COLUMN_INDEX; index <= ConstantHolder::REMOVE_TABLEROW_COLUMN_INDEX; index++)
        headerView->resizeSection(index, 100);
}


//nacitanie priecinkov s domacimi ulohami z priecinku kde su ulozene do tabulky v aplikacii
void MainWindow::loadDirectoriesFromHomeworksFolder(QString& homeworksFolder){
    this->homeworksFolder = homeworksFolder;
    QDir folderPath(homeworksFolder);

    QStringList directoriesList = folderPath.entryList(QDir::Dirs | QDir::NoDotAndDotDot);

    for(QString& directory : directoriesList){
        QString folder = folderPath.filePath(directory);
        addRecordToTable(folder);
    }
}


//pridanie zaznamu domacej ulohy do tabulky domacich uloh
void MainWindow::addRecordToTable(const QString& directory){
    addEmptyRowToTable();
    const int rowIndex = this->numberOfTableRows - 1;

    int positionPersonalInfo = directory.size() - directory.lastIndexOf("/") - 1;
    QString personalInfo = directory.right(positionPersonalInfo);

    addPersonalInfoToRow(rowIndex, personalInfo);

    this->ui->homeWorksTable->setItem(rowIndex, ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX, createCentralizedItem(directory));
    setPointsColumn(rowIndex);
    addReportIconToRow(rowIndex);
    addTestIconToRow(rowIndex);
    addDeleteIconToRow(rowIndex);
}


//pridanie prazdneho riadku do tabulky domacich uloh
void MainWindow::addEmptyRowToTable(){
    this->numberOfTableRows++;
    this->ui->homeWorksTable->setRowCount(this->numberOfTableRows);
}


//pridanie osobnych informacii do riadku tabulky domacich uloh
void MainWindow::addPersonalInfoToRow(const int row, const QString& personalInfo){
    addNameToRow(row, personalInfo);
    addIdToRow(row, personalInfo);
}


//pridanie mena do riadku tabulky domacich uloh
void MainWindow::addNameToRow(int row, const QString& personalInfo){
    int positionName = personalInfo.lastIndexOf("_");

    QString name = personalInfo.left(positionName);
    name.replace("_"," ");

    this->ui->homeWorksTable->setItem(row, ConstantHolder::NAME_COLUM_INDEX, createCentralizedItem(name));
}


//pridanie ais id do riadku tabulky domacich uloh
void MainWindow::addIdToRow(int row, const QString& personalInfo){
    int positionName = personalInfo.lastIndexOf("_");
    int positionId = personalInfo.size() - positionName - 1;

    QString id = personalInfo.right(positionId);
    this->ui->homeWorksTable->setItem(row, ConstantHolder::ID_COLUMN_INDEX, createCentralizedItem(id));
}


//nastavenie prazdneho policka pre body
void MainWindow::setPointsColumn(int rowIndex){
    QTableWidgetItem* item = new QTableWidgetItem();
    item->setFlags(item->flags() | Qt::ItemIsEditable);
    item->setTextAlignment(Qt::AlignCenter);
    item->setText("");
    this->ui->homeWorksTable->setItem(rowIndex, ConstantHolder::POINTS_COLUMN_INDEX, item);
}


//nastevenie policka pre report
void MainWindow::addReportIconToRow(const int rowIndex){
    addIconToCell(QStyle::SP_FileDialogEnd, "report", rowIndex, ConstantHolder::REPORT_COLUMN_INDEX);
}


//nastevenie policka pre testovanie ulohy v danom riadku tabulky domacich uloh
void MainWindow::addTestIconToRow(const int rowIndex){
    addIconToCell(QStyle::SP_BrowserReload, "test", rowIndex, ConstantHolder::TEST_COLUMN_INDEX);
}

//nastevenie policka pre zmazanie ulohy z daneho riadku tabulky domacich uloh
void MainWindow::addDeleteIconToRow(const int rowIndex){
    addIconToCell(QStyle::SP_TrashIcon, "delete", rowIndex, ConstantHolder::REMOVE_TABLEROW_COLUMN_INDEX);
}


//pridane ikony do policka tabulky domacich uloh
void MainWindow::addIconToCell(QStyle::StandardPixmap&& qStyle, QString&& text, const int row, const int column){
    QIcon icon;
    QSize sz(16, 16);

    icon.addPixmap(style()->standardIcon(qStyle).pixmap(sz), QIcon::Normal);

    QTableWidgetItem *iconItem = new QTableWidgetItem;
    iconItem->setText(text);
    iconItem->setIcon(icon);
    iconItem->setFlags(iconItem->flags() & (~Qt::ItemIsEditable));

    this->ui->homeWorksTable->setItem(row, column, iconItem);
}


//vytvori a vrati QTableWidgetItem s vycentrovanym textom
QTableWidgetItem* MainWindow::createCentralizedItem(const QString& text){
    QTableWidgetItem* item = new QTableWidgetItem(text);

    item->setTextAlignment(Qt::AlignCenter);
    item->setFlags(item->flags() & (~Qt::ItemIsEditable));

    return item;
}


//nacita body z CSV suboru ak je nejaky prideleny uzivatelom
void MainWindow::loadPointsFromCsv(const QString& csvFilePath){
    if(csvFilePath == "")
        return;

    QFile csvFile(csvFilePath);
    csvFile.open(QFile::ReadOnly);
    QTextStream csvFileStream(&csvFile);

    while(!csvFileStream.atEnd()){
        QString line = csvFileStream.readLine();
        writePointsFromLine(line);
    }
}

//z precitaneho 1 riadku csv (jeden zapis domacej ulohy v csv) precitaj ziskane body a pridel ich
//v aplikacii do tabulky domacich uloh k prislusnej domacej ulohe
void MainWindow::writePointsFromLine(QString& line){
    line.remove("\"");
    QStringList parsedLine = line.split(";");
    QString points = parsedLine.at(4);
    QString aisId = parsedLine.at(5);

    QList<QTableWidgetItem *> itemList = this->ui->homeWorksTable->findItems(aisId, Qt::MatchExactly);


    //pokial sa v tabulke nenachadza domaca uloha z csv (bola pridana doplnkovo z ineho priecinku)
    //tak ju pridaj do tabulky na zaklade cesty k priecinku ulozenom v csv
    if(itemList.isEmpty()){
        QString homeworkPath = parsedLine.last();
        addRecordToTable(homeworkPath);
        itemList = this->ui->homeWorksTable->findItems(aisId, Qt::MatchExactly);
    }

    //teraz ked tam je domaca uloha najdena v csv urcite, zapis ku nej body do tabulky domacich uloh v aplikacii
    for(QTableWidgetItem*& item : itemList){
        if(item->column() == ConstantHolder::ID_COLUMN_INDEX){
            int rowIndex = item->row();
            this->ui->homeWorksTable->item(rowIndex, ConstantHolder::POINTS_COLUMN_INDEX)->setText(points);
        }
    }
}


//slot vdaka ktoremu moze uzivatel pridat dalsie domace ulohy (napriklad z ineho priecinku)
void MainWindow::on_pcAddButton_clicked(){
    if(isAnythingRunning())
        return;

    QString folderName = QFileDialog::getExistingDirectory(this, "Choose Folder", QDir::homePath(), QFileDialog::ShowDirsOnly);

    if(folderName == "")
        return;

    if(isDirectoryInTable(folderName)){
        QMessageBox::warning(this, "PTester", "You can not add directory which is already in table!");
        return;
    }

    addRecordToTable(folderName);
}


//zisti ci domaca uloha s danou cestou uz je v tabulke domacich uloh pridana
bool MainWindow::isDirectoryInTable(const QString& newDirectory){
    for(int rowIndex = 0; rowIndex < this->numberOfTableRows; rowIndex++){
        QString directory = this->ui->homeWorksTable->item(rowIndex, ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX)->text();
        if(directory == newDirectory)
            return true;
    }

    return false;
}


//slot pre dvojkliknutie v tabulke domacich uloh
void MainWindow::on_homeWorksTable_cellDoubleClicked(const int row, const int column){
    if(column == ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX)
        openFolderFromTableCell(row);
    else if(column == ConstantHolder::REMOVE_TABLEROW_COLUMN_INDEX)
        removeRow(row);
    else if(column == ConstantHolder::REPORT_COLUMN_INDEX)
        openReport(row);
    else if(column == ConstantHolder::TEST_COLUMN_INDEX){
        testOneHomework(row);
    }
}


//otestuj jednu domacu ulohu z daneho riadku tabulky domacich uloh
void MainWindow::testOneHomework(int row){
    if(isAnythingRunning())
        return;

    if(this->pTestPath == ""){
        QMessageBox::warning(this, "PTester", "You must choose .ptest file!");
        return;
    }

    prepareTestingManager();
    this->ui->homeWorksTable->setSortingEnabled(false);

    if(this->testingRunning)
        this->testingManager->startTesting(row);
}


//slot pre kliknutie na testAllButton a otestovanie vsetkych domacich uloh
void MainWindow::on_testAllButton_clicked(){
    if(isAnythingRunning())
        return;

    if(this->ui->homeWorksTable->rowCount() < 1){
        QMessageBox::warning(this, "PTester", "There are NO homeworks!");
        return;
    }

    if(this->pTestPath == ""){
        QMessageBox::warning(this, "PTester", "You must choose .ptest file!");
        return;
    }

    prepareTestingManager();
    this->ui->homeWorksTable->setSortingEnabled(false);

    if(this->testingRunning)
        this->testingManager->startTestingAll();
}


//priprav testingManager na testovanie
void MainWindow::prepareTestingManager(){
    this->testingRunning = true;
    this->ui->stopTestingButton->show();

    this->testingManager->loadPTest(this->pTestPath);
}


//otvor priecinok domacej ulohy z daneho riadku tabulky domacich uloh
void MainWindow::openFolderFromTableCell(const int row){
    QTableWidgetItem* item = this->ui->homeWorksTable->item(row, ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX);
    QString directory = item->text();

    if(!fileExists(directory)){
        QMessageBox::warning(this, "PTester", "Path doesnt exist!");
        return;
    }

    QUrl urlDirection = QUrl::fromLocalFile(directory);
    QDesktopServices::openUrl( urlDirection );
}


//zisti ci subor s danou cestou existuje
bool MainWindow::fileExists(QString& path) {
    QFileInfo checkFile(path);

    return checkFile.exists();
}


//odstrani zadany riadok z tabulky domacich uloh
void MainWindow::removeRow(const int row){
    if(isAnythingRunning())
        return;

    this->ui->homeWorksTable->removeRow(row);
    this->numberOfTableRows--;
}


//otvori html report v dialog-u pre domacu ulohu zo zadaneho riadku tabulky domacich uloh
void MainWindow::openReport(const int row){
    QString reportDirectory = this->ui->homeWorksTable->item(row, ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX)->text();
    QString reportName = reportDirectory.split("/").last() + ConstantHolder::getREPORT_PATH_END();
    QString reportPath = reportDirectory + "/" + reportName;

    QFileInfo reportFileInfo(reportPath);

    if(!reportFileInfo.exists() || !reportFileInfo.isFile()){
        QMessageBox::warning(this, "PTester", "Report doesnt exist!");
        return;
    }

    this->reportDialog->show(reportPath);
}


//slot pre kliknutie na deleteAllButton tlacidlo a zmazanie vsetkych domacich uloh z tabulky domacich uloh
void MainWindow::on_deleteAllButton_clicked(){
    if(isAnythingRunning())
        return;

    QMessageBox::StandardButton reply =
            QMessageBox::question(this, "PTester", "All homeworks will be removed from table.\nAre you sure ?",
                                  QMessageBox::Yes | QMessageBox::No);

    if(reply == QMessageBox::Yes){
        this->numberOfTableRows = 0;
        this->ui->homeWorksTable->setRowCount(this->numberOfTableRows);
    }
}

//slot pre kliknutie na saveButton tlacidlo a otvorenie dialogu pre nastavenie ukladania
void MainWindow::on_saveButton_clicked(){
    if(isAnythingRunning())
        return;

    if(this->ui->homeWorksTable->rowCount() < 1){
        QMessageBox::warning(this, "PTester", "There are NO homeworks!");
        return;
    }

    if(pointsAreMissing()){
        QMessageBox::warning(this, "PTester", "Some points are missing or in bad format");
        return;
    }

    this->saveSettingsDialog->setLastSettings();
    this->saveSettingsDialog->show();
}


//zisti ci nejake body chybaju
bool MainWindow::pointsAreMissing(){
    for(int rowIndex = 0; rowIndex < this->numberOfTableRows; rowIndex++){
        QString points = this->ui->homeWorksTable->item(rowIndex, ConstantHolder::POINTS_COLUMN_INDEX)->text();

        if(points.isEmpty())
            return true;

        if(!qstringIsNumber(points))
            return true;
    }

    return false;
}


//zisti ci qstring je tvoreny len cislami
bool MainWindow::qstringIsNumber(QString qstring){
    bool ok;
    qstring.toDouble(&ok);

    return ok;
}


//ulozenie vysledkov testovania do csv suboru
//slot ktory sa zavola potom co saveSettingsDialog po uzivatelovom nastaveni ukladania, vy-emituje signal saveInputsAccepted
void MainWindow::saveResults(){
    QString folderPath = this->saveSettingsDialog->getFolder();
    QString fileName = this->saveSettingsDialog->getFileName();
    ResultSaver* resultSaver = new ResultSaver(folderPath, fileName);

    int subjectId = this->saveSettingsDialog->getId_predmetu();
    resultSaver->setSubjectId(subjectId);

    int archiveId = this->saveSettingsDialog->getId_archu();
    resultSaver->setArchiveId(archiveId);

    int aisColumnId = this->saveSettingsDialog->getId_sloupec();
    resultSaver->setAisColumnId(aisColumnId);

    this->savingRunning = true;

    resultSaver->setProgressBar(this->ui->progressBar);
    resultSaver->saveResultsFrom(*this->ui->homeWorksTable);

    this->savingRunning = false;
    QMessageBox::information(this, "PTester", "Saving is finished!");
}


//slot pre zmenu textu v searchBox vyhladavaci
//na zaklade vlozeneho textu sa zvyraznia vyhladavane riadky v tabulke domacich uloh
void MainWindow::on_searchBox_textChanged()
{
    this->ui->homeWorksTable->setSelectionMode(QAbstractItemView::MultiSelection);

    QString searched = this->ui->searchBox->text();
    this->ui->homeWorksTable->clearSelection();
    if(searched.isEmpty())
        return;

    QList<QTableWidgetItem *> itemList = this->ui->homeWorksTable->findItems(searched, Qt::MatchContains);

    for(QTableWidgetItem*& item : itemList){
        if(item->column() == ConstantHolder::NAME_COLUM_INDEX || item->column() == ConstantHolder::ID_COLUMN_INDEX)
            this->ui->homeWorksTable->selectRow(item->row());
    }
}


//pri kliknuti na hocijake policko v tabulke domacich uloh, treba toto policko zvyraznit samostatne
//taktiez treba premazat vyhladavac
void MainWindow::on_homeWorksTable_cellClicked(int row, int column)
{
    this->ui->homeWorksTable->setSelectionMode(QAbstractItemView::SingleSelection);
    this->ui->searchBox->clear();
    this->ui->homeWorksTable->selectRow(row);
}


//override-nuta funkcia closeEvent
//pokial uzivatel suhlasi so zavretim okna, vy-emituje sa signal closed
void MainWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "PTest Creator",
                                                                tr("Are you sure ?"),
                                                                QMessageBox::No | QMessageBox::Yes);

    if (resBtn == QMessageBox::No) {
        event->ignore();
    } else {
        emit closed();
        event->accept();
    }
}


// slot pre kliknutie na wrongHomeworksButton tlacidlo
//tvori sa wrongFormatedFilesDialog okno kde sa zobrazia vsetky subory ktore nezodpovedaju spravnemu formatovaniu uloh
void MainWindow::on_wrongHomeworksButton_clicked()
{
    this->wrongFormatedFilesDialog->openWithFolder(this->homeworksFolder);
}


//slot pre kliknutie na ptestChooserButton tlacidlo cez ktore si zvolime aky ptest chcem pouzit pre testovanie
void MainWindow::on_ptestChooserButton_clicked()
{
    if(isAnythingRunning())
        return;

    QString pTestPath = QFileDialog::getOpenFileName(this, "Choose .ptest file with tests", QDir::homePath(), tr("ptest (*.ptest)"));
    if(pTestPath == "")
        return;

    this->ui->label->setText("Actual ptest: " + pTestPath);
    this->pTestPath = pTestPath;
}


//zisti ci testovanie alebo ukladanie bezi a informuje o tom uzivatela
bool MainWindow::isAnythingRunning(){
    if(this->testingRunning){
        QMessageBox::warning(this, "PTester", "Testing is running!");
        return true;
    }

    if(this->savingRunning){
        QMessageBox::warning(this, "PTester", "Saving is running!");
        return true;
    }

    return false;
}


//slot pre kliknutie na stopTestingButton tlacidlo ktore zastavi testovanie
void MainWindow::on_stopTestingButton_clicked()
{
    this->testingManager->stopAllThreads();
    this->testingRunning = false;
}
