#include "constantholder.h"
#include "ptestingthread.h"

#include <QDir>
#include <QFileInfo>
#include <QDebug>
#include <QCoreApplication>
#include <QTimer>
#include <QThread>

PTestingThread::PTestingThread(QTableWidget *testTable, int *nextHomeworkIndex)
{
    this->testTable = testTable;
    this->nextHomeworkIndex = nextHomeworkIndex;
    this->testThis = true;
    this->testNext = true;
    this->resultStrictScenario = true;

    this->mutex = new QMutex();

    connect(this, SIGNAL(readyForNextFile(void)), this, SLOT(run(void)));
    connect(this, SIGNAL(readyForNextTest(void)), this, SLOT(takeNextTest(void)));
}

PTestingThread::~PTestingThread()
{

    delete this->reportStream;
    delete this->mutex;

    this->allScenarios.clear();

    delete this->report;
}

void PTestingThread::loadScenarios(QVector<PTestScenario*>& allScenarios)
{
    this->allScenarios = allScenarios;
}


//nacita sa kontrolny vector neotestovanych scenarov podla vektoru vsetkych scenarov
//WaitingScenariosVector je pomocny vector pre testovanie
void PTestingThread::loadWaitingScenariosVector(){
    this->activeScenarioIndex = 0;
    this->activeTestIndex = 0;

    this->waitingScenarios.clear();

    for(PTestScenario*& scenario : this->allScenarios){
        int numberOfScenarioTests = scenario->getNumberOfTests();
        loadWaitingScenarioTestsVector(numberOfScenarioTests);
    }
}


//nacitanie testov z testovacieho scenara do
void PTestingThread::loadWaitingScenarioTestsVector(int numberOfScenarioTests){
    QVector<int> waitingScenarioTests;

    for(int index = 0; index < numberOfScenarioTests; index++)
        waitingScenarioTests.push_back(index);

    this->waitingScenarios.push_back(waitingScenarioTests);
}


//spusti testovanie len pre jednu domacu ulohu
void PTestingThread::runOne(){
    this->testNext = false;

    run();
}


//spusti testovanie pre vsetky domace ulohy
void PTestingThread::run()
{  
    this->forcedStop = false;

    setIndex();
    if(this->actualHomeworkIndex >= this->testTable->rowCount() || !this->testThis)
        return;

    loadWaitingScenariosVector();
    setPoints();

    setPaths();
    testHomework();
}


//nastavi vsetko spojene s bodovanim na zaciatocne defaultne hodnoty pred kazdym testom domacej ulohy
void PTestingThread::setPoints(){
    this->points = 0;
    this->maxPTestPoints = 0;
    this->scenarioPoints = 0;
    this->resultStrictScenario = true;

    for(PTestScenario*& scenario : this->allScenarios)
        this->maxPTestPoints += scenario->getScenarioPoints();
}


//zisti ktora neotestovana domaca uloha je najblizsie volna (zdielana info medzi vlaknami)
//posunie dalsim vlaknam informaciu o tom ktora domaca uloha bude pre nich najblizsia volna
//z dovodu zdielanej pamata potrebujeme tuto cast kodu zamknut mutex-om
void PTestingThread::setIndex(){
    this->mutex->lock();

    this->actualHomeworkIndex = *this->nextHomeworkIndex;
    *this->nextHomeworkIndex += 1;

    this->mutex->unlock();
}


//nastavi pred testovanim vsetky potrebne cesty
void PTestingThread::setPaths(){
    this->homeworkPath = this->testTable->item(this->actualHomeworkIndex, ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX)->text();
    this->mainPath = this->homeworkPath + ConstantHolder::getMAIN_C_PATH_END();
}


//zacne testovat domacu ulohu
void PTestingThread::testHomework(){
    QFileInfo checkFolder(this->homeworkPath);

    if(checkFolder.exists() && checkFolder.isDir()){
        openReport();
        testHomeworkMain();
    }
    else{
        writeErrorPoints();
        testNextFile();
    }
}


//otvorenie alebo vytvorenie html reportu pre domacu ulohu v jej priecinku, aby bol mozny zapis
void PTestingThread::openReport(){
    QString reportName = this->homeworkPath.split("/").last() + "_" + ConstantHolder::getREPORT_HTML();
    QString reportPath = this->homeworkPath + "/" + reportName;

    this->report = new QFile(reportPath);
    this->reportStream = new QTextStream(this->report);

    this->report->open(QIODevice::WriteOnly);

    QString name = this->homeworkPath.section('/', -1);

    *this->reportStream << "<!DOCTYPE html><html><head><title>" + name + "</title>"
                           + "<style>h2{border-top: 3px solid black;}"
                           + "h3{border: 3px solid black;}"
                           + "table td{vertical-align: top; white-space: pre-line}"
                           + ".pass{background-color: #7FFF00;}"
                           + ".fail{background-color: red;}"
                           + ".warning{background-color: yellow;}"
                           + ".points{text-decoration: underline;}"
                           + ".outputs{background-color: rgba(190,190,190,0.5);}</style>"
                           + "</head><body><h1>" + name + "</h1>";
}


//otestuj main.c pokial existuje
void PTestingThread::testHomeworkMain(){
    QFileInfo checkFileC(this->mainPath);
    if(!checkFileC.exists() || !checkFileC.isFile()){
        this->mainPath.replace(ConstantHolder::getMAIN_C_PATH_END(), ConstantHolder::getMAIN_CPP_PATH_END());
        testHomeworkMainCPP();
    }
    else
        testExistingHomework();
}


//otestuj main.cpp pokial existuje
void PTestingThread::testHomeworkMainCPP(){
    QFileInfo checkFileCPP(this->mainPath);
    if(!checkFileCPP.exists() || !checkFileCPP.isFile()){
        addMissingToReport();
        closeReport();
        writeValidPoints();
        testNextFile();
    }
    else
        testExistingHomework();
}


//otestuj domacu ulohu
//sem sa dostane program len ked existuje main.c alebo main.cpp
//tu prebehne kompilacia programu
void PTestingThread::testExistingHomework(){
    createCompileArgument();
    compileTest();
}


//vytvori argumenty pre kompilator g++ aby sme mohli domacu ulohu skompilovat
void PTestingThread::createCompileArgument(){
    this->arguments.clear();

    //argumenty -Wall a -Wextra nam zabezpecia vypisanie warningov
    //-o oznacuje, ze string nasledujuci za bude output programu
    this->arguments << "-Wall" << "-Wextra" << "-o";

    //meno .exe suboru ktory chceme vytvorit
    QString exePath = this->homeworkPath + ConstantHolder::getMAIN_EXE_PATH_END();
    this->arguments << QDir::toNativeSeparators(exePath);

    //zdrojove kody potrebne na kompilaciu
    QDir directory(this->homeworkPath);
    QStringList allCodeFiles = directory.entryList(QStringList() << "*.c" << "*.C" << "*.cpp" << "*.CPP",QDir::Files);

    for(QString& codeFile : allCodeFiles){
        QString codeFilePath = this->homeworkPath + "/" + codeFile;
        this->arguments << QDir::toNativeSeparators(codeFilePath);
    }
}


//spusti test kompilovatelnosti (pokusi sa o kompilaciu)
void PTestingThread::compileTest(){
    //pre skompilovanim vymaze main.exe ak existuje
    QFile homework(this->homeworkPath + ConstantHolder::getMAIN_EXE_PATH_END());
    homework.remove();

    this->process = new QProcess();

    this->process->setProgram("g++");
    this->process->setArguments(this->arguments);

    connect(this->process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(handleCompileTest(void)));

    this->process->start();
}


//slot ktory sa zavola ked sa dokonci QProcess ktory ma na starost kompilovanie domacej ulohy
void PTestingThread::handleCompileTest(){

    //pokial je vynutene zastavenie testovania
    if(this->forcedStop){
        delete this->process;
        emit actualTestingHomework();
        return;
    }


    interpretCompileTest();
}


//interpretuj vysledky testu kompilovatelnosti
void PTestingThread::interpretCompileTest(){
    if(errorOccured()){
        addCompilationFailedToReport();
        finishHomeworkTesting();
    }

    else{
        addCompilationPassToReport();
        this->isNewScenario = true;
        emit readyForNextTest();
    }

}


//zisti ci pri kompilovani nevznikol error, tym ze skontroluje ci sa vytvoril main.exe (pred kompilovanim bol vymazany, ak daky existoval)
bool PTestingThread::errorOccured(){
    QFileInfo homework (this->homeworkPath + ConstantHolder::getMAIN_EXE_PATH_END());

    if(homework.exists() && homework.isFile())
        return false;

    return true;
}


//slot ktory sa zavola po vy-emitovani signalu readyForNextTest
//pokial su k dispozicii nejake testovacie scenare, spusti pripravu na dalsi test
void PTestingThread::takeNextTest(){
    if(this->waitingScenarios.isEmpty())
        finishHomeworkTesting();
    else
        prepareForNextTest();
}


//priprava pre nasledujuci test, pokial v testovacom scenari je nejaky test
void PTestingThread::prepareForNextTest(){
    if(this->waitingScenarios.first().isEmpty())
        takeNextScenario();
    else{
        this->activeTestIndex = this->waitingScenarios.first().first();
        this->waitingScenarios.first().pop_front();

        startTest();
    }
}


//pripravi nasledujuci testovaci scenar na testovanie
void PTestingThread::takeNextScenario(){
    if(!this->resultStrictScenario)
        this->scenarioPoints = 0;

    PTestScenario* scenario = this->allScenarios.at(this->activeScenarioIndex);
    double maxScenarioPoints = scenario->getScenarioPoints();

    writeScenarioPointsToHtml(maxScenarioPoints);
    addScenarioPoints();

    this->isNewScenario = true;
    this->activeScenarioIndex++;
    this->waitingScenarios.pop_front();

    emit readyForNextTest();
}


//zapise ziskane body do html reportu
void PTestingThread::writeScenarioPointsToHtml(double maxScenarioPoints){
    *this->reportStream << "<h4 class = 'points'>Points for scenario: " << this->scenarioPoints << "/" << maxScenarioPoints << "</h4>";
}

//pripocitaj body z testovacieho scenara ku celkovym ziskanym bodom domacje ulohy
void PTestingThread::addScenarioPoints(){
    this->points += this->scenarioPoints;
    this->scenarioPoints = 0;
    this->resultStrictScenario = true;
}


//spusti test
void PTestingThread::startTest(){
    this->process = new QProcess();
    this->process->setProcessChannelMode(QProcess::SeparateChannels);

    setTestProcess();
    executeTestProcess();
}


//nastav vsetko pred spustenim domacej ulohy vratane argumentov na vstupe
void PTestingThread::setTestProcess(){
    PTestScenario* scenario = this->allScenarios.at(this->activeScenarioIndex);
    PTestScenarioTest* test = scenario->getTest(this->activeTestIndex);

    QString switchersText = test->getSwitchers();
    QStringList switchers = switchersText.split(QRegExp("\\s+"), QString::SkipEmptyParts);

    this->process->setProgram(this->homeworkPath + ConstantHolder::getMAIN_EXE_PATH_END());
    this->process->setArguments(switchers);
}


//vykonaj test domacej ulohy
//vpodstate spusti main.exe s nastavenymi argumentami a posun do vnutra inputy
void PTestingThread::executeTestProcess(){
    PTestScenario* scenario = this->allScenarios.at(this->activeScenarioIndex);
    PTestScenarioTest* test = scenario->getTest(this->activeTestIndex);

    int maxTime = scenario->getMaxTime() * 1000;
    initTimer(maxTime);

    createAllConnections();
    clearProcessOtuputs();

    //spracovanie testovacich inputov(rozdelenie)
    QString inputText = test->getInputs();
    QStringList splittedInputs = inputText.split("\\n");
    splittedInputs.removeLast();

    //spustit domacu ulohu, po dokonceni sa vola slot na zaklade vytvorenej connection
    this->process->start(QIODevice::Unbuffered | QIODevice::ReadWrite);

    //vlozenie vsetkych inputov
    for(QString& input : splittedInputs){
        this->process->write((input.toStdString() + "\n").c_str());
        this->process->waitForBytesWritten(maxTime);
    }

    //zapisanie "newline character-u" pre pripad ze niektore domace ulohy pre ukoncenie musia zadat enter bez vstupu
    this->process->write("\n");
}


//vytvorenie vsetkych connections medzi signalmi a slotmi
void PTestingThread::createAllConnections(){
    connect(this->process, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(testProcessFinished(int, QProcess::ExitStatus)));
    connect(this->process, SIGNAL(readyReadStandardOutput(void)), this, SLOT(saveOutput(void)));
    connect(this->process, SIGNAL(readyReadStandardError(void)), this, SLOT(saveError(void)));
}


//vycitenie vystupov
void PTestingThread::clearProcessOtuputs(){
    this->errorText.clear();
    this->outputText.clear();
}


//inicializacia timer-u na odstopovanie casu vykonavania testu domacej ulohy
//ak vykonavanie trva prilis dlho, ukonci testovanie a ohodnot 0 bodmi
void PTestingThread::initTimer(int maxTime){
    this->killtimer = new QTimer(this->process);
    this->killtimer->setInterval(maxTime);
    this->killtimer->setSingleShot(true);

    connect(this->killtimer, SIGNAL(timeout()), this, SLOT(terminateProcess()));
    connect(this->process, SIGNAL(started()), this->killtimer, SLOT(start()));
}


//zabije proces
void PTestingThread::terminateProcess(){
    this->process->kill();
}


//slot zavolany po emitovani signalu finished
//ukonci sa testovanie aj casovac a vola funkciu na spracovanie vysledkov
void PTestingThread::testProcessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{  
    this->killtimer->stop();
    delete this->killtimer;

    if(this->forcedStop){
        delete this->process;
        emit actualTestingHomework();
        return;
    }

    if(exitStatus == QProcess::NormalExit)
        handleTestResult(true);
    else
        handleTestResult(false);
}


//ulozi standardny vystup
void PTestingThread::saveOutput(){
    this->outputText = this->process->readAllStandardOutput();
}


//ulozi chybovy vystup
void PTestingThread::saveError(){
    this->errorText = this->process->readAllStandardError();
}


//spracuvava vysledky testu
void PTestingThread::handleTestResult(bool result){
    finishQProcess();

    PTestScenario* scenario = this->allScenarios.at(this->activeScenarioIndex);
    PTestScenarioTest* test = scenario->getTest(this->activeTestIndex);

    writeScenarioHead(scenario->getName());

    QString expectedOutput = test->getOutputs();
    transformOutput(expectedOutput);

    //zapise vysledky dokonceneho testu do html reportu
    double gainedPoints;
    if(result && (QString::compare(this->outputText, expectedOutput) == 0)){
        *this->reportStream << "<h4 class = 'pass'>TEST " << this->activeTestIndex << " RESULT: PASS</h4>";
        gainedPoints = test->getPoints();
    }
    else{
        *this->reportStream << "<h4 class = 'fail'>TEST " << this->activeTestIndex << " RESULT: FAIL</h4>";
        gainedPoints = 0;
        if(scenario->getStrict())
            this->resultStrictScenario = false;
    }


    //do html reportu zapise vstupne argumenty, inputy a ocakavane outputy s realnymi outputmi
    *this->reportStream << "<p>arguments: " + test->getSwitchers() + "</p>";
    writeInputsToReport(QString(test->getInputs()));
    writeOutputsToReport(expectedOutput);
    writeTestPointsToReport(gainedPoints, test->getPoints());

    emit readyForNextTest();
}


//zapisanie vsetkych input do html reportu
void PTestingThread::writeInputsToReport(QString&& inputs){
    //spracovanie inputov do vhodnej podoby
    inputs.replace("\\n", "\n");
    QString temporaryInput;
    QTextStream inputStream(&inputs);

    *this->reportStream << "<table><tr><td>inputs: </td><td>";

    //zapis kazdeho inputu na samostatny riadok
    while(!inputStream.atEnd()){
        temporaryInput = inputStream.readLine();
        *this->reportStream  << "<pre><span>" + temporaryInput + "</span></pre>";
    }

    *this->reportStream << "</td></tr></table>";

}


//zapisanie outputov do html reportu
void PTestingThread::writeOutputsToReport(QString& expectedOutput){
    //spracovanie outputov
    QString temporaryOutput;
    QTextStream expectedOutputStream(&expectedOutput);
    QTextStream realOutputStream(&this->outputText);

    *this->reportStream << "<table>";

    //vypis ocakavanych outputov (kazdy do vlastneho riadku)
    *this->reportStream << "<tr><td>expected output:</td> <td>";
    while(!expectedOutputStream.atEnd()){
        temporaryOutput = expectedOutputStream.readLine();
        temporaryOutput.remove("\n");
        *this->reportStream << "<pre><span  class='outputs'>" + temporaryOutput + "</span></pre>";
    }
    *this->reportStream << "</td></tr>";

    //vypis realnych outputov (kazdy do vlastneho riadku)
    *this->reportStream << "<tr><td>homework output:</td> <td>";
    while(!realOutputStream.atEnd()){
        temporaryOutput = realOutputStream.readLine();
        temporaryOutput.remove("\n");
        *this->reportStream << "<pre><span  class='outputs'>" + temporaryOutput + "</span></pre>";
    }
    *this->reportStream << "</td></tr>";
    *this->reportStream << "</table>";

    //vypis chyboveho vystupu
    this->errorText = this->process->readAllStandardError();
    if(!errorText.isEmpty())
        *this->reportStream << "<p>error: " + this->errorText + "<p>";
}


//dokonci qprocess zavretim kanalu na citanie a transformovanim outputu do potrebneho tvaru
void PTestingThread::finishQProcess(){
    this->process->closeWriteChannel();

    transformOutput(this->outputText);
}


//zapise do html reportu hlavicku scenaru
void PTestingThread::writeScenarioHead(const QString& scenarioName){
    if(this->isNewScenario){
        *this->reportStream << "<h2>SCENARIO: " + scenarioName + "</h2>";
        this->isNewScenario = false;
    }
}


//zapise do html reportu
void PTestingThread::writeTestPointsToReport(double points, double maxPoints){
    this->scenarioPoints += points;
    *this->reportStream << "<p class = 'points'>Points for test: " << points << "/" << maxPoints << "</p>";
}


//transformuj output do pozadovaneho formatu
void PTestingThread::transformOutput(QString& outputText){
    //ak je output prilis dlhy, skrat ho na 1000 znakov
    int maxOutputLength = outputText.length();
    if(maxOutputLength > 1000){
        maxOutputLength = 1000;
        outputText = outputText.left(maxOutputLength);
    }

    //spravne zapisane "new line characters"
    if(outputText.contains("\r\n"))
        outputText.replace("\r\n", "\n");
    if(outputText.contains("\\n"))
        outputText.replace("\\n", "\n");
    if(!outputText.endsWith("\n"))
        outputText.append("\n");
    if(outputText.contains("\n\n"))
        outputText.replace("\n\n", "\n");
}


//dokonci testovanie domacej ulohy
void PTestingThread::finishHomeworkTesting(){
    closeReport();
    writeValidPoints();

    testNextFile();
}


//zapise do html reportu neuspesny pokus o kompilaciu a zapisanie errorov
void PTestingThread::addCompilationFailedToReport(){
    if(!this->report->isOpen())
        return;

    *this->reportStream << "<p class='fail'>compilation: FAILED</p>";

    this->errorText = this->process->readAllStandardError();
    QStringList allErrors = this->errorText.split("\n");

    *this->reportStream << "<p class='warning'>ERRORS:</p>";
    for(QString& error : allErrors)
        *this->reportStream << "<p class='fail'>" + error + "</p>";

    this->process->kill();
    this->process->close();
    delete this->process;
}


//zapise ze chybal main
void PTestingThread::addMissingToReport(){
    if(!this->report->isOpen())
        return;

    *this->reportStream << "<p class='fail'>MISSING MAIN</p>";
}


//zapise do html reportu uspesny pokus o kompilaciu a zapisanie warningov
void PTestingThread::addCompilationPassToReport(){
    if(!this->report->isOpen())
        return;

    *this->reportStream << "<p class='pass'>compilation: PASS</p>";

    this->errorText = this->process->readAllStandardError();
    QStringList allWarnings = this->errorText.split("\n");

    if(allWarnings.size() == 0)
        return;

    *this->reportStream << "<p class='warning'>WARNINGS:</p>";
    for(QString& warning : allWarnings)
        *this->reportStream << "<p class='warning'>" + warning + "</p>";

    this->process->kill();
    this->process->close();
    delete this->process;
}


//zapisanie finalneho vysledku a zatvorenie html reportu
void PTestingThread::closeReport(){
    if(this->report->isOpen()){
        *this->reportStream << "<h2>Final Result: "  << this->points << " points / " << this->maxPTestPoints << " maximum points</h2>";
        *this->reportStream << "</body></html>";
        this->report->close();
    }
}


//zapisanie bodov(skutocne cislo) do tabulky domacich uloh
void PTestingThread::writeValidPoints(){
    QString points = QString::number(this->points);
    writePoints(points);
}


//zapisanie ze chyba folder do policka bodmi, ak bol nahodou folder po nacitani uloh do tabulky vymazany z pocitaca
void PTestingThread::writeErrorPoints(){
    writePoints("Missing folder");
}


//zapisanie bodov do policka body v tabulke domacich uloh
//je potrebne znovu mutexom zamknut kvoli pristupovaniu ku kodu so zdielanou pamatou medzi vlaknami
void PTestingThread::writePoints(QString pointsInfo){
    this->mutex->lock();
    this->testTable->item(this->actualHomeworkIndex, ConstantHolder::POINTS_COLUMN_INDEX)->setText(pointsInfo);
    QCoreApplication::processEvents();  //v pripade mozne zamrznutia aplikacie, tymto prikazom sa vykonaju veci v event-loope ktore cakaju na vykonanie
    this->mutex->unlock();
}


//emitovanim signalu chod na testovanie dalsej domacej ulohy a PTestingManagerovi daj signal o dokonceni jednej domacej ulohy
void PTestingThread::testNextFile(){
    this->testThis = this->testNext;

    this->mutex->lock();
    emit actualTestingHomework();
    this->mutex->unlock();

    if(this->testNext)
        emit readyForNextFile();
}


//zabi vsetko
//tato cast a vseobecne vsetko okolo vynuteneho zastavenia testovania uzivatelom by si zasluzilo velku prerabku...pri zastaveni niekedy pada
void PTestingThread::killEverything(){
    this->forcedStop = true;

    try{
        this->process->kill();
    } catch (const std::exception& e) { /*NOTHING*/ }
}
