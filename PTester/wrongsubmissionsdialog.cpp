#include "wrongsubmissionsdialog.h"
#include "ui_wrongsubmissionsdialog.h"

#include <QDir>
#include <QDebug>

WrongSubmissionsDialog::WrongSubmissionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WrongSubmissionsDialog)
{
    ui->setupUi(this);
    initializeTable();
    setWindowFlags(windowFlags() | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
}

WrongSubmissionsDialog::~WrongSubmissionsDialog()
{
    delete ui;
}


//inicializacia responzivnosti tabulky
void WrongSubmissionsDialog::initializeTable(){
    QHeaderView *headerView = this->ui->filesTable->horizontalHeader();

    headerView->setVisible(true);
    headerView->setFrameStyle(QFrame::Box | QFrame::Plain);
    headerView->setLineWidth(1);

    headerView->setSectionResizeMode(QHeaderView::Stretch);

    this->ui->filesTable->setEditTriggers(QAbstractItemView::NoEditTriggers);
}


//otvorenie dialogu so subormi zo zadaneho priecinku
void WrongSubmissionsDialog::openWithFolder(QString& homeworksDirectory){
    this->ui->filesTable->setRowCount(0);
    this->lastFileIndex = 0;

    loadWrongFormatedFiles(homeworksDirectory);
    this->ui->searchBox->setText("");

    this->show();
}


//do tabulky nacita vsetky neplatne subory
void WrongSubmissionsDialog::loadWrongFormatedFiles(QString& homeworksDirectory){
    QDir folderPath(homeworksDirectory);

    QStringList filesList = folderPath.entryList(QDir::Files | QDir::NoDotAndDotDot);

    for(QString& fileName : filesList)
        addToTable(fileName);
}


//do tabulky neplatnych suborov prida subor iba ak nema koncovku .csv alebo .ptest
void WrongSubmissionsDialog::addToTable(QString& fileName){
    if(fileName.endsWith(".csv") || fileName.endsWith(".ptest"))
        return;

    QTableWidgetItem* fileItem = new QTableWidgetItem(fileName);


    this->ui->filesTable->setRowCount(this->lastFileIndex + 1);
    this->ui->filesTable->setItem(this->lastFileIndex, 0, fileItem);
    this->lastFileIndex++;
}


//slot pre zmenu textu v searchBox vyhladavacom inpute
//zvyraznia sa riadky ktore zodpovedaju vyhladavaniu
void WrongSubmissionsDialog::on_searchBox_textChanged()
{
    this->ui->filesTable->setSelectionMode(QAbstractItemView::MultiSelection);

    QString id = this->ui->searchBox->text();
    this->ui->filesTable->clearSelection();
    if(id.isEmpty())
        return;

    QList<QTableWidgetItem *> itemList = this->ui->filesTable->findItems(id, Qt::MatchContains);

    for(QTableWidgetItem*& item : itemList)
            this->ui->filesTable->selectRow(item->row());

}


//slot pre kliknutie na policko v tabulke suborov
//vymaze sa searchBox a zvyrazni sa len zakliknuty riadok
void WrongSubmissionsDialog::on_filesTable_cellClicked(int row, int column)
{
    this->ui->filesTable->setSelectionMode(QAbstractItemView::SingleSelection);
    this->ui->searchBox->clear();
    this->ui->filesTable->selectRow(row);
}
