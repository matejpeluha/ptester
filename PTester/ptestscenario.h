#ifndef PTESTSCENARIO_H
#define PTESTSCENARIO_H

#include <QString>
#include <QVector>

#include "ptestscenariotest.h"

/*---------------------
 trieda ktora reprezentuje testovaci scenar s ulozenymi testami
---------------------*/

class PTestScenario
{
public:
    PTestScenario();
    ~PTestScenario();

    const QString& getName() const;
    void setName(QString& name);

    bool getStrict() const;
    void setStrict(bool value);

    int getMaxTime() const;
    void setMaxTime(int value);

    int getNumberOfTests() const;

    PTestScenarioTest* getTest(int index);
    void addTest(PTestScenarioTest* test);
    double getScenarioPoints();

private:
    QString name;   //nazov scenara
    bool strict;    //vyjadruje ci je scenar strict (ak ano tak musia vsetky testy prejst po ziskanie bodov)
    int maxTime;    //maximalny cas na vykonanie testu v scenari
    double scenarioPoints;  //celkove body za vsetky testy v scenari

    int numberOfTests;  //pocet testov
    QVector<PTestScenarioTest*> allScenarioTests;   //vsetky testy v scenari

    void incrementNumberOfTests();
};

#endif // PTESTSCENARIO_H
