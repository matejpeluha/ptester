#ifndef WRONGFORMATEDFILESDIALOG_H
#define WRONGFORMATEDFILESDIALOG_H

#include <QDialog>

namespace Ui {
class WrongSubmissionsDialog;
}

/*---------------------
 trieda pre uvodne okno(dialog) obshaujuce vsetky subory z priecinku domacich uloh, ktore nie su zabalene v inom priecinku
 s vynimkou .ptest a .csv suborov
---------------------*/

class WrongSubmissionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WrongSubmissionsDialog(QWidget *parent = nullptr);
    ~WrongSubmissionsDialog();

    void openWithFolder(QString &homeworksDirectory);
private slots:
    void on_searchBox_textChanged();

    void on_filesTable_cellClicked(int row, int column);

private:
    Ui::WrongSubmissionsDialog *ui;
    int lastFileIndex;  //index posledneho suboru v tabulke suborov

    void loadWrongFormatedFiles(QString &homeworksDirectory);
    void addToTable(QString &file);
    void initializeTable();
};

#endif // WRONGFORMATEDFILESDIALOG_H
