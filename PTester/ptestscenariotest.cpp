#include "ptestscenariotest.h"

PTestScenarioTest::PTestScenarioTest()
{
    this->switchers = "";
    this->inputs = "";
    this->outputs = "";
    this->points = 0;
}

const QString& PTestScenarioTest::getSwitchers() const
{
    return this->switchers;
}

void PTestScenarioTest::setSwitchers(QString& value)
{
    this->switchers = value;
}

const QString& PTestScenarioTest::getInputs() const
{
    return this->inputs;
}

void PTestScenarioTest::setInputs(QString& value)
{
    this->inputs = value;
}

const QString& PTestScenarioTest::getOutputs() const
{
    return this->outputs;
}

void PTestScenarioTest::setOutputs(QString& value)
{
    this->outputs = value;
}

double PTestScenarioTest::getPoints() const
{
    return this->points;
}

void PTestScenarioTest::setPoints(double value)
{
    this->points = value;
}

