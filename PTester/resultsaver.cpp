#include "constantholder.h"
#include "resultsaver.h"

#include <QDir>
#include <QFile>
#include <QTextStream>

#include <QDebug>


ResultSaver::ResultSaver(QString& folderPath, QString& fileName)
{
    this->folderPath = folderPath;
    this->fileName = fileName;
}


//hlavna funkcia na ulozenie vysledkov z tabulky domacich uloh
void ResultSaver::saveResultsFrom(QTableWidget &homeworksTable)
{
    initProgressBar(homeworksTable.rowCount());
    openCsvFile();

    int numberOfRows = homeworksTable.rowCount();

    for(int rowIndex = 0; rowIndex < numberOfRows; rowIndex++){
        fillEveryId();

        QString points = homeworksTable.item(rowIndex, ConstantHolder::POINTS_COLUMN_INDEX)->text();
        *this->csvFileStream << "\"" << points + "\";";

        QString aisId = homeworksTable.item(rowIndex, ConstantHolder::ID_COLUMN_INDEX)->text();
        *this->csvFileStream << "\"" << aisId + "\";";

        QString name = homeworksTable.item(rowIndex, ConstantHolder::NAME_COLUM_INDEX)->text();
        *this->csvFileStream << "\"" << name << "\";";

        QString homeworkPath = homeworksTable.item(rowIndex, ConstantHolder::HOMEWORK_PATH_COLUMN_INDEX)->text();
        *this->csvFileStream << "\"" << homeworkPath << "\"\n";
    }

    this->progressBar->close();
    closeCsvFile();
}


//inicializuj progress bar na defaultne hodnoty do stavu ako ma zacat
void ResultSaver::initProgressBar(int maximum){
    this->progressBar->setMinimum(0);
    this->progressBar->setMaximum(maximum);
    this->progressBar->setValue(0);
    this->progressBar->setOrientation(Qt::Horizontal);

    this->progressBar->show();
}


//otvor alebo vytvor csv subor
void ResultSaver::openCsvFile(){
    QDir dir;

    if (!dir.exists(this->folderPath))
        dir.mkpath(this->folderPath);

    setCsvFile();
}


//nastav csv subor a vytvor ho spolu so streamom k nemu
void ResultSaver::setCsvFile(){
    QString csvFileName = this->folderPath + "/" + this->fileName;

    this->csvFile = new QFile(csvFileName);
    this->csvFile->open(QFile::WriteOnly);

    this->csvFileStream = new QTextStream(this->csvFile);
}


//vyplnenie vsetkych podstatnych id ktore ais pozaduje
void ResultSaver::fillEveryId(){
    fillStudyId();
    fillSubjectId();
    fillArchiveId();
    fillAisColumnId();
}

void ResultSaver::fillStudyId(){
    *this->csvFileStream << "\""<< "000000" << "\";";
}

void ResultSaver::fillSubjectId(){
    *this->csvFileStream << "\"" << this->subjectId << "\";";
}

void ResultSaver::fillArchiveId(){
    *this->csvFileStream << "\"" <<  this->archiveId << "\";";
}

void ResultSaver::fillAisColumnId(){
    *this->csvFileStream << "\"" << this->aisColumnId << "\";";
}


//zavri csv file a premaz stream aj objekt reprezentujuci subor
void ResultSaver::closeCsvFile(){
    delete this->csvFileStream;

    this->csvFile->close();
    delete this->csvFile;
}

void ResultSaver::setSubjectId(int subjectId){
    this->subjectId = subjectId;
}

void ResultSaver::setArchiveId(int archiveId){
    this->archiveId = archiveId;
}

void ResultSaver::setAisColumnId(int value)
{
    this->aisColumnId = value;
}

void ResultSaver::setProgressBar(QProgressBar *value)
{
    progressBar = value;
}

