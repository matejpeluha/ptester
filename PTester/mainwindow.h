#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProgressDialog>

#include "ptestingmanager.h"
#include "reportdialog.h"
#include "savesettingsdialog.h"
#include "ui_mainwindow.h"
#include "wrongsubmissionsdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

/*---------------------
 trieda pre hlavnu cast programu s tabulkou vsetkych domacich uloh
---------------------*/

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr, QString* homeworksFolder = nullptr);
    ~MainWindow();
    virtual void closeEvent(QCloseEvent *event);

    void loadPointsFromCsv(const QString &csvFilePath);
private slots:
    void on_pcAddButton_clicked();

    void on_homeWorksTable_cellDoubleClicked(int row, int column);

    void on_deleteAllButton_clicked();

    void on_testAllButton_clicked();


    void on_saveButton_clicked();

    void on_searchBox_textChanged();

    void on_homeWorksTable_cellClicked(int row, int column);

    void on_wrongHomeworksButton_clicked();

    void on_ptestChooserButton_clicked();

    void handleTestingFinished();

    void on_stopTestingButton_clicked();

    void saveResults();

private:
    Ui::MainWindow *ui;

    //dalsie otvaratelne okna
    ReportDialog* reportDialog;
    WrongSubmissionsDialog* wrongFormatedFilesDialog;
    SaveSettingsDialog* saveSettingsDialog;

    //obstaravanie testovania domacich uloh
    PTestingManager* testingManager;

    //potrebne pomocne atributy triedy
    QString pTestPath; //cesta k aktivnemu .ptest suboru
    QString homeworksFolder; //priecinok so vsetkymi domacimi ulohami
    int numberOfTableRows;  //pocet riadkov tabulky s domacimi ulohami

    //premenne vyjadrujuce ci bezi testovanie a ukladanie
    bool testingRunning;
    bool savingRunning;

    void initializeSideAttributes();
    void initializeDialogs();
    void initializeHomeWorksTable();
    void setSectionSizes();
    void loadDirectoriesFromHomeworksFolder(QString& homeworksFolder);

    void addRecordToTable(const QString& directory);
    void addEmptyRowToTable();
    void removeRow(const int row);

    void addPersonalInfoToRow(const int row, const QString& personalInfo);

    QTableWidgetItem* createCentralizedItem(const QString& text);

    bool isDirectoryInTable(const QString& newDirectory);

    void addDeleteIconToRow(const int rowIndex);
    void addTestIconToRow(const int rowIndex);
    void addReportIconToRow(const int rowIndex);
    void addIconToCell(QStyle::StandardPixmap&& qStyle, QString&& text, const int row, const int column);

    void openFolderFromTableCell(const int row);
    bool fileExists(QString& path);

    void openReport(const int row);

    void addNameToRow(int row, const QString &personalInfo);
    void addIdToRow(int row, const QString &personalInfo);
    void testOneHomework(int row);
    void initializeTestingManager();
    void prepareTestingManager();
    bool isAnythingRunning();
    void writePointsFromLine(QString &line);

    void setPointsColumn(int rowIndex);
    bool pointsAreMissing();

    bool qstringIsNumber(QString qstring);
signals:
    void closed();  //signal emitujuci sa pri zatvoreni MainWindow
};
#endif // MAINWINDOW_H
