/********************************************************************************
** Form generated from reading UI file 'wrongsubmissionsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WRONGSUBMISSIONSDIALOG_H
#define UI_WRONGSUBMISSIONSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_WrongSubmissionsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLineEdit *searchBox;
    QTableWidget *filesTable;

    void setupUi(QDialog *WrongSubmissionsDialog)
    {
        if (WrongSubmissionsDialog->objectName().isEmpty())
            WrongSubmissionsDialog->setObjectName(QString::fromUtf8("WrongSubmissionsDialog"));
        WrongSubmissionsDialog->setWindowModality(Qt::NonModal);
        WrongSubmissionsDialog->resize(1077, 528);
        WrongSubmissionsDialog->setWindowOpacity(1.000000000000000);
        WrongSubmissionsDialog->setStyleSheet(QString::fromUtf8("QDialog {\n"
"	color: rgb(0, 0 ,0);\n"
"    background-color: rgb(30, 30, 30);\n"
"}\n"
"\n"
"#filesTable {\n"
"border: no;\n"
"   color: rgb(255, 255, 255);\n"
"    background-color: rgb(100, 100, 100);\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(60, 60, 60), stop: 1 rgb(30, 30, 30));\n"
"}\n"
"\n"
""));
        WrongSubmissionsDialog->setSizeGripEnabled(false);
        verticalLayout = new QVBoxLayout(WrongSubmissionsDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        searchBox = new QLineEdit(WrongSubmissionsDialog);
        searchBox->setObjectName(QString::fromUtf8("searchBox"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(searchBox->sizePolicy().hasHeightForWidth());
        searchBox->setSizePolicy(sizePolicy);
        searchBox->setMinimumSize(QSize(300, 0));

        verticalLayout->addWidget(searchBox);

        filesTable = new QTableWidget(WrongSubmissionsDialog);
        if (filesTable->columnCount() < 1)
            filesTable->setColumnCount(1);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setBackground(QColor(255, 255, 255));
        filesTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        filesTable->setObjectName(QString::fromUtf8("filesTable"));
        filesTable->setStyleSheet(QString::fromUtf8("QTableCornerButton, QTableCornerButton::section{border:no;background-color:rgb(70,70,70);}\n"
"QHeaderView, QHeaderView::section {color:white; background-color:rgb(70, 70, 70);}\n"
"QTableView::item:selected{background-color: rgba(200,200,200,150); color: black };\n"
"\n"
"\n"
""));
        filesTable->horizontalHeader()->setVisible(true);
        filesTable->horizontalHeader()->setHighlightSections(false);
        filesTable->verticalHeader()->setVisible(false);

        verticalLayout->addWidget(filesTable);


        retranslateUi(WrongSubmissionsDialog);

        QMetaObject::connectSlotsByName(WrongSubmissionsDialog);
    } // setupUi

    void retranslateUi(QDialog *WrongSubmissionsDialog)
    {
        WrongSubmissionsDialog->setWindowTitle(QCoreApplication::translate("WrongSubmissionsDialog", "Wrong Formated Files", nullptr));
        searchBox->setPlaceholderText(QCoreApplication::translate("WrongSubmissionsDialog", "File name", nullptr));
        QTableWidgetItem *___qtablewidgetitem = filesTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("WrongSubmissionsDialog", "File", nullptr));
    } // retranslateUi

};

namespace Ui {
    class WrongSubmissionsDialog: public Ui_WrongSubmissionsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WRONGSUBMISSIONSDIALOG_H
