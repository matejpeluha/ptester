#ifndef CONSTANTHOLDER_H
#define CONSTANTHOLDER_H

#include <QString>


//trieda s konstantami ktore vyuzivam napriec celou aplikaciou
class ConstantHolder
{
public:
    ConstantHolder();

    //funckie vracajuce stringy

    //symboly (tag-y) pre .ptest subory
    static QString getSCENARIO_SYMBOL();
    static QString getSTRICT_SCENARIO_SYMBOL();
    static QString getTEST_SYMBOL();
    static QString getSCENARIO_END_SYMBOL();
    static QString getTEST_END_SYMBOL() ;
    static QString getSWITCHERS_SYMBOL() ;
    static QString getINPUT_SYMBOL() ;
    static QString getOUTPUT_SYMBOL() ;
    static QString getTIMER_SYMBOL() ;

    //nazvy main suborov (koncovky c, cpp, exe)
    static QString getMAIN_CPP_PATH_END() ;
    static QString getMAIN_C_PATH_END() ;
    static QString getMAIN_EXE_PATH_END() ;

    //nazvy pre html report
    static QString getREPORT_HTML() ;
    static QString getREPORT_PATH_END() ;

    //vypis prejdenia/neprejdenia testu
    static QString getPASSED_COMPILED_TEST() ;
    static QString getFAILED_COMPILED_TEST() ;

    //nazov pre csv result subor
    static QString getCSV_RESULTS_STRING() ;


    //ciselne konstanty(nie su potrebne funkcie)
public:

    //indexy stlpcov v tabulke so vsetkymi ulohami v MainWindow
    static const int NAME_COLUM_INDEX = 0;
    static const int ID_COLUMN_INDEX = 1;
    static const int HOMEWORK_PATH_COLUMN_INDEX = 2;
    static const int POINTS_COLUMN_INDEX = 3;
    static const int REPORT_COLUMN_INDEX = 4;
    static const int TEST_COLUMN_INDEX = 5;
    static const int REMOVE_TABLEROW_COLUMN_INDEX = 6;

    //indexy stlpcov v tabulke s testovacimi scenarmi v PTestCreatorWindow
    static const int SCENARIO_NAME_COLUMN_INDEX = 0;
    static const int TESTS_COLUMN_INDEX = 1;
    static const int STRICT_COLUMN_INDEX = 2;
    static const int REMOVE_SCENARIO_COLUMN_INDEX = 3;

    //indexy stlpcov v tabulke s testami v scenari v PTestScenarioDialog
    static const int SWITCHERS_COLUMN_INDEX = 0;
    static const int INPUT_COLUMN_INDEX = 1;
    static const int OUTPUT_COLUMN_INDEX = 2;
    static const int TIME_COLUMN_INDEX = 4;
    static const int REMOVE_COLUMN_INDEX = 5;


};

#endif // CONSTANTHOLDER_H
