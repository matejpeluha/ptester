#ifndef STARTWINDOW_H
#define STARTWINDOW_H

#include <QDialog>
#include "mainwindow.h"
#include "ptestcreatorwindow.h"

namespace Ui {
class StartWindow;
}

/*---------------------
 trieda pre uvodne okno(dialog) obshaujuce zakladne menu
---------------------*/

class StartWindow : public QDialog
{
    Q_OBJECT

public:
    explicit StartWindow(QWidget *parent = nullptr);
    ~StartWindow();

public slots:
    void closeMainWindowSlot();
    void closePTestCreatorSlot();
private slots:
    void on_newLoaderButton_clicked();

    void on_checkedLoaderButton_clicked();

    void on_pTestCreatorButton_clicked();

    void on_pTestCreatorLoadButton_clicked();

private:
    Ui::StartWindow *ui;
    MainWindow* mainWindow; //hlavne okno v ktorom su nacitane vsetky testy a prebieha tam testovanie
    PTestCreatorWindow* pTesterCreatorWindow;   //okno v ktorom sa da ju vytvarat a upravovat .ptest subory urcene na testovanie

    bool qmessageAskYesNo(const QString& question);
    void startPTestCreator(QString &pTestPath);
    void setHeaderImage();

    void showMainWindow(QString &homeworksFolderPath, const QString& csvFilePath = "");
};

#endif // STARTWINDOW_H
