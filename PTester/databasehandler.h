#ifndef DATABASEHANDLER_H
#define DATABASEHANDLER_H
#include <QSqlDatabase>
#include <QTableWidget>
#include <QSqlQueryModel>

class DatabaseHandler
{
public:
    DatabaseHandler(const QString& directory);
    ~DatabaseHandler() = default;
    void openDatabase();
    int saveTable(const QTableWidget& table);
    void closeDatabase();
    void loadTableFromDatabase(QTableView& homeworksTable);
private:
    QString directory;
    QSqlDatabase database;
    const int NAME_COLUM_INDEX = 0;
    const int ID_COLUMN_INDEX = 1;
    const int DIRECTORY_COLUMN_INDEX = 2;
    const int POINTS_COLUMN_INDEX = 3;
    const int REPORT_COLUMN_INDEX = 4;
    const QString DATABASE_PATH_END = "\\database.sqlite";

    int saveAllRows(const QTableWidget& table);
    void saveRow(int row, const QTableWidget& table);
    void setDatabaseName();
    void deleteTable();
    void createTable();
    int savingPointsError();

};

#endif // DATABASEHANDLER_H
