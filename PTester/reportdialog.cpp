#include "reportdialog.h"
#include "ui_reportdialog.h"

#include <QDesktopServices>
#include <QFile>
#include <QDebug>

ReportDialog::ReportDialog(QWidget *parent) : QDialog(parent), ui(new Ui::ReportDialog){
    ui->setupUi(this);
    setWindowTitle("Report");
    setWindowFlags(windowFlags() | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
}

ReportDialog::~ReportDialog()
{
    delete this->ui;
}


//otvor dialog na zaklade cesty k reportu
void ReportDialog::show(const QString& reportPath){
    this->reportPath = reportPath;
    this->reportPath.replace("\\","/");

    sendHtmlToTextBrowser();

    QDialog::show();
}


//nastavenie textu z .html suboru do textBrowseru v aplikacii
//frontendovo nie je zobrazovanie html reportu v prehliadaci optimalizovane a zobrazuje sa velmi neprehladne
void ReportDialog::sendHtmlToTextBrowser(){
    QFile report(this->reportPath);
    report.open(QIODevice::ReadOnly);

    this->ui->textBrowser->setText(report.readAll());

    report.close();
}


//slot pre kliknutie na openInternetButton tlacidlo vdaka comu vieme otvorit html report priamo v internetovom prehliadaci
void ReportDialog::on_openInternetButton_clicked(){
    QDesktopServices::openUrl(this->reportPath);
}
