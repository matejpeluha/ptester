/********************************************************************************
** Form generated from reading UI file 'ptestcreatorwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PTESTCREATORWINDOW_H
#define UI_PTESTCREATORWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_PTestCreatorWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLabel *pTestPathLabel;
    QTableWidget *scenariosTable;
    QHBoxLayout *horizontalLayout;
    QPushButton *addScenarioButton;
    QPushButton *saveAllTestsButton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *PTestCreatorWindow)
    {
        if (PTestCreatorWindow->objectName().isEmpty())
            PTestCreatorWindow->setObjectName(QString::fromUtf8("PTestCreatorWindow"));
        PTestCreatorWindow->resize(1331, 767);
        PTestCreatorWindow->setStyleSheet(QString::fromUtf8("QMainWindow {\n"
"	color: rgb(0, 0 ,0);\n"
"    background-color: rgb(70, 70, 70);\n"
"}\n"
"\n"
"QTableWidget {\n"
"border: no;\n"
"   color: rgb(255, 255, 255);\n"
"    background-color: rgb(100, 100, 100);\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(140, 140, 140), stop: 1 rgb(70, 70, 70));\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton {\n"
"    border: 2px solid #8f8f91;\n"
"    border-radius: 6px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 1 rgb(160, 160, 160), stop: 0 #dadbde);\n"
"	min-width: 80px;\n"
"	min-height: 30px;\n"
"	max-width: 450px;\n"
"}\n"
"\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:hover { color: rgb(80, 80, 80);}"));
        centralwidget = new QWidget(PTestCreatorWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy);
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pTestPathLabel = new QLabel(centralwidget);
        pTestPathLabel->setObjectName(QString::fromUtf8("pTestPathLabel"));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        QBrush brush1(QColor(120, 120, 120, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        pTestPathLabel->setPalette(palette);
        QFont font;
        font.setPointSize(13);
        pTestPathLabel->setFont(font);
        pTestPathLabel->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(pTestPathLabel);

        scenariosTable = new QTableWidget(centralwidget);
        if (scenariosTable->columnCount() < 4)
            scenariosTable->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setBackground(QColor(0, 0, 0));
        scenariosTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        scenariosTable->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        scenariosTable->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        scenariosTable->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        scenariosTable->setObjectName(QString::fromUtf8("scenariosTable"));
        scenariosTable->setStyleSheet(QString::fromUtf8("QTableCornerButton, QTableCornerButton::section{border:no;background-color:rgb(70,70,70);}\n"
"QHeaderView, QHeaderView::section {color:white; background-color:rgb(70, 70, 70);}\n"
"QTableView::item:selected{background-color: rgba(200,200,200,150); color: black };\n"
""));
        scenariosTable->verticalHeader()->setVisible(false);

        verticalLayout->addWidget(scenariosTable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(30);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 0);
        addScenarioButton = new QPushButton(centralwidget);
        addScenarioButton->setObjectName(QString::fromUtf8("addScenarioButton"));

        horizontalLayout->addWidget(addScenarioButton);

        saveAllTestsButton = new QPushButton(centralwidget);
        saveAllTestsButton->setObjectName(QString::fromUtf8("saveAllTestsButton"));

        horizontalLayout->addWidget(saveAllTestsButton);


        verticalLayout->addLayout(horizontalLayout);

        PTestCreatorWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(PTestCreatorWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1331, 26));
        PTestCreatorWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(PTestCreatorWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        statusbar->setSizeGripEnabled(true);
        PTestCreatorWindow->setStatusBar(statusbar);

        retranslateUi(PTestCreatorWindow);

        QMetaObject::connectSlotsByName(PTestCreatorWindow);
    } // setupUi

    void retranslateUi(QMainWindow *PTestCreatorWindow)
    {
        PTestCreatorWindow->setWindowTitle(QCoreApplication::translate("PTestCreatorWindow", "MainWindow", nullptr));
        pTestPathLabel->setText(QString());
        QTableWidgetItem *___qtablewidgetitem = scenariosTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("PTestCreatorWindow", "Scenario", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = scenariosTable->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("PTestCreatorWindow", "Tests", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = scenariosTable->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("PTestCreatorWindow", "Strict", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = scenariosTable->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("PTestCreatorWindow", "REMOVE", nullptr));
        addScenarioButton->setText(QCoreApplication::translate("PTestCreatorWindow", "Add New Scenario", nullptr));
        saveAllTestsButton->setText(QCoreApplication::translate("PTestCreatorWindow", "Save All Tests", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PTestCreatorWindow: public Ui_PTestCreatorWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PTESTCREATORWINDOW_H
