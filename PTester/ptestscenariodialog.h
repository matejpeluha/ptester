#ifndef TESTSCENARIODIALOG_H
#define TESTSCENARIODIALOG_H

#include <QDialog>
#include <QFile>
#include <QMap>
#include <QProcess>
#include <QSpinBox>
#include <QTextStream>

namespace Ui {
class PTestScenarioDialog;
}

/*---------------------
 trieda pre okno(dialog) reprezentujuca jeden testovaci scenar so vsetkymi jeho testami
---------------------*/

class PTestScenarioDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PTestScenarioDialog(QWidget *parent = nullptr);
    ~PTestScenarioDialog();

    void openWithName(QString &scenarioName);
    void saveTestsInFile(QTextStream* pTestStream);
    void loadScenario(QTextStream* pTestStream);

    int getMaxTime();

private slots:
    void on_addTestButton_clicked();

    void on_testsTable_cellDoubleClicked(int row, int column);

    void on_generateOutputButton_clicked();

    void on_testsTable_cellChanged(int row, int column);

private:
    Ui::PTestScenarioDialog *ui;

    int numberOfTests;  //pocet testov
    QString sampleExecutable;   //.exe subor cez ktory viem vygenerovat outputy
    QTextStream* pTestStream;   //stream do .ptest suboru

    QVector<QDoubleSpinBox*> allSpinBoxes;  //vsetky spinboxy, kazdy spinbox vyjadruje pocet bodov za spravne vykonany test

    void setUpUI();
    void setSectionSizes();
    void addRemoveItem();
    void addPointsBox();
    void removeRow(int row);
    void testSwitchersAndInput(int inputIndex);
    void addEmptyRow();
    void setEmptyTableCells();
    void setUpSpinBox(QDoubleSpinBox *spinBox);
    void writeTestHeaderToFile(int testIndex);
    void writeTestBodyToFile(int testIndex);

    void loadScenarioTimer(QString &line);
    void loadTest(QString &line);
    void loadTestAttributes();
    void writeInfoToTest(QString &line, int column);

    void startProcess(QProcess &process);

    void transformAllInputs(QVector<const char *> &allInputs, QString &inputText);

    void formatOutput(QString &output);
};

#endif // TESTSCENARIODIALOG_H
