#ifndef PTESTINGMANAGER_H
#define PTESTINGMANAGER_H

#include "ptestingthread.h"
#include "ptestscenario.h"

#include <QProgressBar>
#include <QTableWidget>
#include <QThread>
#include <QElapsedTimer>


/*---------------------
 trieda ktora riadi testovanie domacich uloh
---------------------*/

class PTestingManager : public QObject
{
    Q_OBJECT
public:
    PTestingManager(QTableWidget* testTable);
    ~PTestingManager();
    void loadPTest(QString& ptestPath);
    void startTesting(int rowIndex);
    void startTestingAll();

    void setProgressBar(QProgressBar *progressBar);
    void stopAllThreads();
private:
    bool forcedStop; //hovori o tom ci je zastavenie testovania vynutene(true) alebo prirodzene ked otestuje vsetko(false)
    bool allowFinishTesting; //hovori o tom ci mozeme dokoncit testovanie

    QElapsedTimer timer;    //na meranie casu testovania

    QProgressBar* progressBar;  //progress bar ktory ukazuje kolko percent domacich uloh je skontrlovanych
    int numberOfTestedHomeworks;    //pocet otestovanych domacich uloh

    QTableWidget* testTable;    //tabulka na testovanie v ktorej su domace ulohy
    int nextTestIndex;  //index najblizsej NEotestovanej domacej ulohy z testTable, ktora ma byt otestovana
    QTextStream* pTestStream;   //stream do suboru .ptest

    QVector<QThread*> allThreads;   //vsetky vlakna QThread
    QVector<PTestingThread*> allPTestingThreads;  //objekty urcene na testovanie uloh, beziace pod roznymi vlaknami
    QVector<PTestScenario*> allScenarios;   //vsetky testovacie scenare

    void cleanPoints(int index);
    void cleanAllPoints();

    int getIdealThreadCount();
    void startAllThreads(int idealThreadCount);
    void startThread();

    void loadAllScenarios(QString& ptestPath);
    void loadPTestScenario(QString &line);
    void setScenario(QString &line);

    void loadScenario();
    void loadScenarioTimer(QString &line);
    void loadTest(QString &line);
    void loadTestAttributes(PTestScenarioTest* test);

    void initProgressBar();
    void initProgressBar(int maximum);
    void deleteVectors();

    bool isTestingFinishedForAllThreads();
private slots:
    void handleProgressBar();

signals:
    void testingFinished(); //emituje sa ked vlakno dokonci testovanie
};

#endif // PTESTINGMANAGER_H
