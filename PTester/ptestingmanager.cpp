#include "constantholder.h"
#include "ptestingmanager.h"

#include <QDebug>
#include <QFileInfo>
#include <QProgressBar>

PTestingManager::PTestingManager(QTableWidget *testTable)
{
    this->testTable = testTable;
    this->nextTestIndex = 0;
    this->allowFinishTesting = true;

    qRegisterMetaType<QVector<int>>("QVector<int>");
    qRegisterMetaType<QList<QPersistentModelIndex>>("QList<QPersistentModelIndex>");
    qRegisterMetaType<QAbstractItemModel::LayoutChangeHint>("QAbstractItemModel::LayoutChangeHint");
}

PTestingManager::~PTestingManager()
{
    deleteVectors();

    delete this->testTable;
}

void PTestingManager::setProgressBar(QProgressBar* progressBar){
    this->progressBar = progressBar;
}


//nacita vsetky testovacie scenare z .ptest suboru
void PTestingManager::loadPTest(QString &ptestPath)
{
    qDeleteAll(this->allScenarios);
    this->allScenarios.clear();

    QFileInfo pTestPathInfo(ptestPath);

    if(pTestPathInfo.exists() && pTestPathInfo.isFile())
        loadAllScenarios(ptestPath);
}

//nacita vsetky testovacie scenare z .ptest suboru
void PTestingManager::loadAllScenarios(QString &ptestPath){
    QFile pTest(ptestPath);
    pTest.open(QIODevice::ReadOnly);

    this->pTestStream = new QTextStream(&pTest);

    while(!this->pTestStream->atEnd()) {
        QString line = this->pTestStream->readLine();
        loadPTestScenario(line);
    }

    pTest.close();
}


//nacita jeden testovaci scenar z .ptest suboru
void PTestingManager::loadPTestScenario(QString& line){
    if(line.startsWith(ConstantHolder::getSCENARIO_SYMBOL())){
        PTestScenario* scenario = new PTestScenario();
        this->allScenarios.push_back(scenario);

        setScenario(line);
    }
}


//nastavi testovaci scenar podla precitaneho riadku z .ptest suboru
void PTestingManager::setScenario(QString& line){
    PTestScenario* scenario = this->allScenarios.last();

    QString scenarioName = line.section(" ", 1, -1);
    scenario->setName(scenarioName);

    if(line.startsWith(ConstantHolder::getSTRICT_SCENARIO_SYMBOL()))
        scenario->setStrict(true);

    loadScenario();
}


//finalne nacitanie testovacieho scenar vratane vsetkych testov
void PTestingManager::loadScenario()
{
   while(!this->pTestStream->atEnd()){
        QString line = this->pTestStream->readLine();
        if(line.startsWith(ConstantHolder::getSCENARIO_END_SYMBOL()))
            break;

        else if(line.startsWith(ConstantHolder::getTIMER_SYMBOL()))
            loadScenarioTimer(line);

        else if(line.startsWith(ConstantHolder::getTEST_SYMBOL()))
            loadTest(line);
    }
}


//nacita timer testovacieho scenaru
void PTestingManager::loadScenarioTimer(QString& line){
    int maxTime = line.section(ConstantHolder::getTIMER_SYMBOL(), 1, 1).toInt();
    this->allScenarios.last()->setMaxTime(maxTime);
}


//nacita test z testovacieho scenaru
void PTestingManager::loadTest(QString& line){
    PTestScenarioTest* test = new PTestScenarioTest();

    double points = line.section(ConstantHolder::getTEST_SYMBOL(), 1, 1).toDouble();
    test->setPoints(points);

    loadTestAttributes(test);

    this->allScenarios.last()->addTest(test);
}


//nacita prepinace, inputy aj outputy vramci jedneho testu
void PTestingManager::loadTestAttributes(PTestScenarioTest* test){
    QString line;

    //premenne urcujuce ci sa v ramci nacitavaneho testu uz nacitala pozadovana cast
    bool switchersApproached = false;
    bool inputApproached = false;
    bool outputApproached = false;

    while(!this->pTestStream->atEnd()){
         line = this->pTestStream->readLine();

         //ak nacital koniec scenara alebo testu
         if(line.startsWith(ConstantHolder::getSCENARIO_END_SYMBOL()) || line.startsWith(ConstantHolder::getTEST_END_SYMBOL()))
             return;

         //ak nacital prepinace
         else if(line.startsWith(ConstantHolder::getSWITCHERS_SYMBOL())){
             QString switchers = line.section(ConstantHolder::getSWITCHERS_SYMBOL(), 1, 1);
             test->setSwitchers(switchers);
             switchersApproached = true;
         }

         //ak nacital inputy
         else if(line.startsWith(ConstantHolder::getINPUT_SYMBOL())){
             QString inputs = line.section(ConstantHolder::getINPUT_SYMBOL(), 1, 1);
             test->setInputs(inputs);
             inputApproached = true;
         }

         //ak nacital outputy
         else if(line.startsWith(ConstantHolder::getOUTPUT_SYMBOL())){
             QString outputs = line.section(ConstantHolder::getOUTPUT_SYMBOL(), 1, 1);
             test->setOutputs(outputs);
            outputApproached = true;
         }

         // ked sa nacitaju prepinace, inputy aj outputy pre test, nacitanie testu konci
         else if(switchersApproached && inputApproached && outputApproached)
             return;
    }
}


//spusti testovanie jednej domacej ulohy na jednom vlakne podla indexu riadku z tabulky
void PTestingManager::startTesting(int rowIndex){
    this->timer.start();

    //vsetky nastavenia
    this->forcedStop = false;
    this->allowFinishTesting = true;
    cleanPoints(rowIndex);
    initProgressBar(1);

    this->nextTestIndex = rowIndex;
    this->numberOfTestedHomeworks = 0;

    //vytvorene a ulozenie vlakna
    QThread* thread = new QThread();
    this->allThreads.push_back(thread);

    PTestingThread* process = new PTestingThread(this->testTable, &this->nextTestIndex);
    process->loadScenarios(this->allScenarios);
    connect(process, SIGNAL(actualTestingHomework(void)), this, SLOT(handleProgressBar(void)));

    this->allPTestingThreads.push_back(process);

    process->moveToThread(thread);

    connect(thread, SIGNAL(started()), process, SLOT(runOne()));

    //spustenie vlakna
    thread->start();
}


//spustenie testovania vsetkych domacich uloh
void PTestingManager::startTestingAll()
{
    this->timer.start();

    this->forcedStop = false;
    this->allowFinishTesting = true;

    this->nextTestIndex = 0;
    this->numberOfTestedHomeworks = 0;
    int idealThreadCount = getIdealThreadCount();

    cleanAllPoints();
    initProgressBar();
    startAllThreads(idealThreadCount);
}

void PTestingManager::initProgressBar(){
    initProgressBar(this->testTable->rowCount());
}

//inicializuje progress bar aby uzivatel videl kolko percent domacich uloh je otestovanych
void PTestingManager::initProgressBar(int maximum){
    this->progressBar->setMinimum(0);
    this->progressBar->setMaximum(maximum);
    this->progressBar->setValue(0);
    this->progressBar->setOrientation(Qt::Horizontal);

    this->progressBar->show();
}


//prejde tabulku domacich uloh a vsade vymaze body
void PTestingManager::cleanAllPoints(){
    for(int rowIndex = 0; rowIndex < this->testTable->rowCount(); rowIndex++)
        cleanPoints(rowIndex);
}


//vymaze body na zadanom riadku tabulky domacich uloh
void PTestingManager::cleanPoints(int rowIndex){
    this->testTable->item(rowIndex, ConstantHolder::POINTS_COLUMN_INDEX)->setText("");
}


//vyrataj idealny pocet vlakien
int PTestingManager::getIdealThreadCount(){
    int idealThreadCount = QThread::idealThreadCount();
    if(idealThreadCount > this->testTable->rowCount())
        idealThreadCount = this->testTable->rowCount();

    return idealThreadCount;
}


//vytvori idealny pocet vlakien a vsetky spusti
void PTestingManager::startAllThreads(int idealThreadCount){
    for(int threadIndex = 0; threadIndex < idealThreadCount; threadIndex++)
        startThread();
}


//vytvori jedno vlakno a spusti ho
void PTestingManager::startThread(){
    QThread* thread = new QThread();
    this->allThreads.push_back(thread);

    PTestingThread* process = new PTestingThread(this->testTable, &this->nextTestIndex);
    process->loadScenarios(this->allScenarios);
    connect(process, SIGNAL(actualTestingHomework(void)), this, SLOT(handleProgressBar(void)));

    this->allPTestingThreads.push_back(process);
    process->moveToThread(thread);

    connect(thread, SIGNAL(started()), process, SLOT(run()));
    thread->start();
}


//slot ktory sa zavola po vy-emitovani signalu actualTestingHomework z vlakna
//tento slot sa vola vzdy ked vlakno dokonci testovanie jednej ulohy
//primarne aktualizuje progressBar
void PTestingManager::handleProgressBar()
{
    this->numberOfTestedHomeworks++;
    this->progressBar->setValue(this->numberOfTestedHomeworks);

    //pokial je testovanie dokoncene
    if(isTestingFinishedForAllThreads()){
        this->allowFinishTesting = false;
        deleteVectors();
        this->progressBar->hide();
        qDebug() << this->timer.elapsed();
        emit testingFinished();
    }

}


//zistuje ci testovanie skoncilo (nezalezi ci prirodzene alebo nutene)
bool PTestingManager::isTestingFinishedForAllThreads(){
    return (this->progressBar->value() == this->progressBar->maximum() || this->forcedStop) && this->allowFinishTesting;
}


//zastav vlakna a vymaz vektory uskladnujuce vsetko potrebne pre vlakna
void PTestingManager::deleteVectors(){
    qDeleteAll(this->allScenarios);

    for(PTestingThread*& pTestingThread : this->allPTestingThreads)
        pTestingThread->thread()->exit();

    for(QThread*& thread : this->allThreads)
        thread->quit();

    qDeleteAll(this->allPTestingThreads);

    this->allScenarios.clear();
    this->allPTestingThreads.clear();
    this->allThreads.clear();
}


//zabi vsetky vlakna
void PTestingManager::stopAllThreads(){
    this->forcedStop = true;

    for(PTestingThread*& pTestingThread : this->allPTestingThreads){
        pTestingThread->killEverything();
    }

}
