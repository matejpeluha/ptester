#ifndef HOMEWORKSFORMATTER_H
#define HOMEWORKSFORMATTER_H

#include <QString>


//trieda na formatovanie domacich uloh

class HomeworksFormatter
{
public:
    HomeworksFormatter();
    void formatHomeworks(QString &homeworksDirectory);
private:
    void wrapToFolder(QString& homeworksDirectory, QString& fileName);
};

#endif // HOMEWORKSFORMATTER_H
