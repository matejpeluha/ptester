#ifndef PTESTSCENARIOTEST_H
#define PTESTSCENARIOTEST_H

#include <QString>


/*---------------------
 trieda reprezentujuca jeden test v testovacom scenari
---------------------*/


class PTestScenarioTest
{
public:
    PTestScenarioTest();

    const QString& getSwitchers() const;
    void setSwitchers(QString& value);

    const QString& getInputs() const;
    void setInputs(QString& value);

    const QString& getOutputs() const;
    void setOutputs(QString& value);

    double getPoints() const;
    void setPoints(double value);

private:
    QString switchers;  //prepinace (vstupne argumenty do domacej ulohy)
    QString inputs; //standardne vstupy
    QString outputs;    //standardne vystupy
    double points;  //body udelene za test
};

#endif // PTESTSCENARIOTEST_H
