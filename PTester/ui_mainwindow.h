/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QGridLayout *gridLayout;
    QPushButton *wrongHomeworksButton;
    QSpacerItem *horizontalSpacer;
    QPushButton *ptestChooserButton;
    QSpacerItem *horizontalSpacer_2;
    QLineEdit *searchBox;
    QTableWidget *homeWorksTable;
    QWidget *buttonWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *pcAddButton;
    QPushButton *deleteAllButton;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *testAllButton;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *saveButton;
    QProgressBar *progressBar;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *stopTestingButton;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1261, 794);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setStyleSheet(QString::fromUtf8("#MainWindow {\n"
"	color: rgb(0, 0 ,0);\n"
"    background-color: rgb(30, 30, 30);\n"
"}\n"
"\n"
"#homeWorksTable {\n"
"border: no;\n"
"   color: rgb(255, 255, 255);\n"
"    background-color: rgb(100, 100, 100);\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(60, 60, 60), stop: 1 rgb(30, 30, 30));\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton {\n"
"    border: 2px solid #8f8f91;\n"
"    border-radius: 6px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 1 rgb(160, 160, 160), stop: 0 #dadbde);\n"
"	min-width: 80px;\n"
"	min-height: 30px;\n"
"	max-width: 450px;\n"
"}\n"
"\n"
"#pcAddButton, #deleteAllButton{\n"
"max-width: 300px;\n"
"min-height: 20px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:hover { color: rgb(80, 8"
                        "0, 80);}"));
        MainWindow->setAnimated(false);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralwidget->sizePolicy().hasHeightForWidth());
        centralwidget->setSizePolicy(sizePolicy1);
        verticalLayout = new QVBoxLayout(centralwidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("QLabel{\n"
"	color: white;\n"
"}"));

        verticalLayout->addWidget(label);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(-1, -1, -1, 0);
        wrongHomeworksButton = new QPushButton(centralwidget);
        wrongHomeworksButton->setObjectName(QString::fromUtf8("wrongHomeworksButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(wrongHomeworksButton->sizePolicy().hasHeightForWidth());
        wrongHomeworksButton->setSizePolicy(sizePolicy2);
        wrongHomeworksButton->setMinimumSize(QSize(254, 34));
        wrongHomeworksButton->setMaximumSize(QSize(254, 34));
        wrongHomeworksButton->setBaseSize(QSize(0, 100));
        wrongHomeworksButton->setLayoutDirection(Qt::RightToLeft);
        wrongHomeworksButton->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	max-width: 250px;\n"
"	min-width: 250px;\n"
"}"));

        gridLayout->addWidget(wrongHomeworksButton, 0, 5, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 4, 1, 1);

        ptestChooserButton = new QPushButton(centralwidget);
        ptestChooserButton->setObjectName(QString::fromUtf8("ptestChooserButton"));
        sizePolicy2.setHeightForWidth(ptestChooserButton->sizePolicy().hasHeightForWidth());
        ptestChooserButton->setSizePolicy(sizePolicy2);
        ptestChooserButton->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	max-width: 250px;\n"
"	min-width: 250px;\n"
"}"));

        gridLayout->addWidget(ptestChooserButton, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

        searchBox = new QLineEdit(centralwidget);
        searchBox->setObjectName(QString::fromUtf8("searchBox"));

        gridLayout->addWidget(searchBox, 0, 3, 1, 1);


        verticalLayout->addLayout(gridLayout);

        homeWorksTable = new QTableWidget(centralwidget);
        if (homeWorksTable->columnCount() < 7)
            homeWorksTable->setColumnCount(7);
        QFont font;
        font.setBold(true);
        font.setUnderline(true);
        font.setWeight(75);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem->setFont(font);
        __qtablewidgetitem->setBackground(QColor(0, 0, 0, 0));
        homeWorksTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem1->setFont(font);
        __qtablewidgetitem1->setBackground(QColor(0, 0, 0, 0));
        homeWorksTable->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        __qtablewidgetitem2->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem2->setFont(font);
        homeWorksTable->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        __qtablewidgetitem3->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem3->setFont(font);
        __qtablewidgetitem3->setBackground(QColor(106, 106, 106));
        homeWorksTable->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        __qtablewidgetitem4->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem4->setFont(font);
        __qtablewidgetitem4->setBackground(QColor(106, 106, 106));
        homeWorksTable->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        __qtablewidgetitem5->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem5->setFont(font);
        homeWorksTable->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        __qtablewidgetitem6->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem6->setFont(font);
        homeWorksTable->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        homeWorksTable->setObjectName(QString::fromUtf8("homeWorksTable"));
        homeWorksTable->setEnabled(true);
        homeWorksTable->setMouseTracking(false);
        homeWorksTable->setStyleSheet(QString::fromUtf8("QTableCornerButton, QTableCornerButton::section{border:no;background-color:rgb(30,30,30);}\n"
"QHeaderView, QHeaderView::section {color:white; background-color:rgb(30, 30, 30);}\n"
"QTableView::item:selected{background-color: rgba(200,200,200,150); color: black };\n"
""));
        homeWorksTable->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::DoubleClicked|QAbstractItemView::EditKeyPressed|QAbstractItemView::SelectedClicked);
        homeWorksTable->setAlternatingRowColors(false);
        homeWorksTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        homeWorksTable->setShowGrid(true);
        homeWorksTable->setGridStyle(Qt::SolidLine);
        homeWorksTable->setSortingEnabled(true);
        homeWorksTable->setCornerButtonEnabled(false);
        homeWorksTable->setColumnCount(7);
        homeWorksTable->horizontalHeader()->setVisible(false);
        homeWorksTable->horizontalHeader()->setCascadingSectionResizes(false);
        homeWorksTable->horizontalHeader()->setHighlightSections(true);
        homeWorksTable->horizontalHeader()->setStretchLastSection(false);
        homeWorksTable->verticalHeader()->setVisible(false);
        homeWorksTable->verticalHeader()->setCascadingSectionResizes(false);
        homeWorksTable->verticalHeader()->setHighlightSections(true);
        homeWorksTable->verticalHeader()->setProperty("showSortIndicator", QVariant(false));
        homeWorksTable->verticalHeader()->setStretchLastSection(false);

        verticalLayout->addWidget(homeWorksTable);

        buttonWidget = new QWidget(centralwidget);
        buttonWidget->setObjectName(QString::fromUtf8("buttonWidget"));
        buttonWidget->setEnabled(true);
        horizontalLayout = new QHBoxLayout(buttonWidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 10);
        pcAddButton = new QPushButton(buttonWidget);
        pcAddButton->setObjectName(QString::fromUtf8("pcAddButton"));

        horizontalLayout->addWidget(pcAddButton);

        deleteAllButton = new QPushButton(buttonWidget);
        deleteAllButton->setObjectName(QString::fromUtf8("deleteAllButton"));

        horizontalLayout->addWidget(deleteAllButton);


        verticalLayout->addWidget(buttonWidget);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(-1, -1, -1, 5);
        testAllButton = new QPushButton(centralwidget);
        testAllButton->setObjectName(QString::fromUtf8("testAllButton"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(testAllButton->sizePolicy().hasHeightForWidth());
        testAllButton->setSizePolicy(sizePolicy3);

        horizontalLayout_2->addWidget(testAllButton);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(-1, -1, 0, 5);
        saveButton = new QPushButton(centralwidget);
        saveButton->setObjectName(QString::fromUtf8("saveButton"));

        horizontalLayout_3->addWidget(saveButton);


        verticalLayout->addLayout(horizontalLayout_3);

        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setStyleSheet(QString::fromUtf8("#progressBar{\n"
"	color: white;\n"
"}"));
        progressBar->setValue(24);

        verticalLayout->addWidget(progressBar);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(-1, -1, -1, 0);
        stopTestingButton = new QPushButton(centralwidget);
        stopTestingButton->setObjectName(QString::fromUtf8("stopTestingButton"));

        horizontalLayout_4->addWidget(stopTestingButton);


        verticalLayout->addLayout(horizontalLayout_4);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1261, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Actual ptest:", nullptr));
        wrongHomeworksButton->setText(QCoreApplication::translate("MainWindow", "Wrong Submissions", nullptr));
        ptestChooserButton->setText(QCoreApplication::translate("MainWindow", "Choose ptest", nullptr));
        searchBox->setPlaceholderText(QCoreApplication::translate("MainWindow", "Name/ID", nullptr));
        QTableWidgetItem *___qtablewidgetitem = homeWorksTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("MainWindow", "Name", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = homeWorksTable->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("MainWindow", "AIS ID", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = homeWorksTable->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("MainWindow", "Path", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = homeWorksTable->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("MainWindow", "Points", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = homeWorksTable->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("MainWindow", "Report", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = homeWorksTable->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("MainWindow", "check", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = homeWorksTable->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("MainWindow", "delete", nullptr));
        pcAddButton->setText(QCoreApplication::translate("MainWindow", "Add One Homework from Computer", nullptr));
        deleteAllButton->setText(QCoreApplication::translate("MainWindow", "Delete All", nullptr));
        testAllButton->setText(QCoreApplication::translate("MainWindow", "Test All Homeworks", nullptr));
        saveButton->setText(QCoreApplication::translate("MainWindow", "Save Results", nullptr));
        stopTestingButton->setText(QCoreApplication::translate("MainWindow", "Stop Testing", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
