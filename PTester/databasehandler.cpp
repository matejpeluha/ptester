#include "databasehandler.h"

#include <QMessageBox>
#include <QDir>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>
#include <QString>


DatabaseHandler::DatabaseHandler(const QString& directory){
    this->directory = directory;
}

void DatabaseHandler::openDatabase(){
    this->database = QSqlDatabase::addDatabase("QSQLITE");
    setDatabaseName();
    this->database.open();
}

void DatabaseHandler::setDatabaseName(){
    QDir homeworksFolder(this->directory);

    if(!homeworksFolder.exists())
        QMessageBox::warning(nullptr, "PTester", this->directory + "\nDirectory doesnt exists anymore!");

    this->database.setDatabaseName(this->directory + this->DATABASE_PATH_END);
}

int DatabaseHandler::saveTable(const QTableWidget& table){
    deleteTable();
    createTable();

    return saveAllRows(table);
}

int DatabaseHandler::saveAllRows(const QTableWidget& table){
    int numberOfRows = table.rowCount();

    for(int rowIndex = 0; rowIndex < numberOfRows; rowIndex++){
        if(table.item(rowIndex, this->POINTS_COLUMN_INDEX) == nullptr)
            return savingPointsError();

        saveRow(rowIndex, table);
    }

    return 0;
}

void DatabaseHandler::createTable(){
    QString createTableQuery = "CREATE TABLE homeworks("
                          "Name VARCHAR(40),"
                          "AIS_ID integer primary key,"
                          "Directory VARCHAR(160),"
                          "Points integer,"
                          "Report VARCHAR(7));";
    QSqlQuery query;
    query.exec(createTableQuery);
}

int DatabaseHandler::savingPointsError(){
    QMessageBox::warning(nullptr, "PTester", "You have to write down points to table!!!\nCheck all homeworks!");
    deleteTable();

    return -1;
}

void DatabaseHandler::deleteTable(){
    QString deleteTableQuery = "DROP TABLE IF EXISTS homeworks;";
    QSqlQuery query;
    query.exec(deleteTableQuery);
}

void DatabaseHandler::saveRow(int row, const QTableWidget& table){
    const QString name = table.item(row, this->NAME_COLUM_INDEX)->text();
    const QString AIS_ID = table.item(row, this->ID_COLUMN_INDEX)->text();
    const QString directory = table.item(row, this->DIRECTORY_COLUMN_INDEX)->text();
    const QString points = table.item(row, this->POINTS_COLUMN_INDEX)->text();

    QString insertQuery = "INSERT INTO homeworks(Name, AIS_ID, Directory, Points, Report)"
                          "VALUES('" + name + "','" + AIS_ID + "','" + directory + "','" + points + "', 'report');";
    QSqlQuery query;
    query.exec(insertQuery);
}

void DatabaseHandler::closeDatabase(){
    QString connection = this->database.connectionName();
    this->database.close();
    this->database = QSqlDatabase();
    QSqlDatabase::removeDatabase(connection);
}

void DatabaseHandler::loadTableFromDatabase(QTableView& homeworksTable){
     QSqlQuery query("SELECT * FROM homeworks");
     QSqlQueryModel* model = new QSqlQueryModel();
     model->setQuery(query);

     homeworksTable.setModel(model);
}
