#ifndef PTESTCREATORWINDOW_H
#define PTESTCREATORWINDOW_H

#include "ptestscenariodialog.h"
#include <QMainWindow>
#include <QFile>
#include <QCheckBox>
#include <QTextStream>

namespace Ui {
class PTestCreatorWindow;
}


/*---------------------
 trieda pre vytvaranie a upravovanie ptestov a jeho testovacich scenarov
---------------------*/

class PTestCreatorWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit PTestCreatorWindow(QWidget *parent = nullptr, const QString& pTestPath = "");
    ~PTestCreatorWindow();

    virtual void closeEvent(QCloseEvent *event);

private slots:
    void on_addScenarioButton_clicked();

    void on_saveAllTestsButton_clicked();

    void on_scenariosTable_cellDoubleClicked(int row, int column);

private:
    Ui::PTestCreatorWindow *ui;

    QString sampleExecutable; //.exe subor na ktorom sa moze dat vygenerovat outputy
    QString pTestPath;  //cesta ku .ptest suboru
    QTextStream* pTestStream;   //stream na zapisovanie do .ptest suboru
    int numberOfScenarios;  //pocet testovacich scenarov v jednom ptest-e
    int lastScenarioNumber; //posledny testovaci scenar - index

    QVector<PTestScenarioDialog*> allScenarios; //vsetky testovacie scenare a ich prisluchajuce okna
    QVector<QCheckBox*> allCheckBoxes;  //vsetky zaskrtavacia checkBoxy oznacujuce "strict" testovacie scenare

    void setSectionSizes();
    void addRemoveItem();

    void testInput(int inputIndex);

    void loadPTestToTable();
    void setUpUI();
    void loadAllScenarios();
    void loadPTestScenario(QString &line);

    void addScenarioItem();
    void addStrictCheckBoxItem();
    void addTestsItem();

    void removeRow(int row);
    void openTestScenarioDialog(int row);

    void saveScenariosInFile(QFile &pTest);
    void saveOneScenarioInFile(int scenarioIndex, QFile &pTest);
    void writeScenarioHeaderToFile(int scenarioIndex, QTextStream &pTest);
    void writeTestsToFile(int scenarioIndex, QTextStream &pTest);

    void createScenario(QString &line);
    void closeAllScenarios();

signals:
    void closed();  //signal vyemitovany pri zatvoreni okna
};

#endif // PTESTCREATORWINDOW_H
