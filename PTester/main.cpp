#include "startwindow.h"

#include <QApplication>

int main(int argc, char *argv[]){
    QApplication app(argc, argv);

    StartWindow startWindow;
    startWindow.show();

    return app.exec();
}
