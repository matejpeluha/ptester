/********************************************************************************
** Form generated from reading UI file 'savesettingsdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SAVESETTINGSDIALOG_H
#define UI_SAVESETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_SaveSettingsDialog
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_5;
    QLabel *folderLabel;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_4;
    QLineEdit *fileNameInput;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QSpinBox *id_predmetuInput;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QSpinBox *id_archuInput;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label_3;
    QSpinBox *id_sloupecInput;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *SaveSettingsDialog)
    {
        if (SaveSettingsDialog->objectName().isEmpty())
            SaveSettingsDialog->setObjectName(QString::fromUtf8("SaveSettingsDialog"));
        SaveSettingsDialog->resize(400, 294);
        verticalLayout = new QVBoxLayout(SaveSettingsDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_5 = new QLabel(SaveSettingsDialog);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy);

        horizontalLayout_5->addWidget(label_5);

        folderLabel = new QLabel(SaveSettingsDialog);
        folderLabel->setObjectName(QString::fromUtf8("folderLabel"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(folderLabel->sizePolicy().hasHeightForWidth());
        folderLabel->setSizePolicy(sizePolicy1);

        horizontalLayout_5->addWidget(folderLabel);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_4 = new QLabel(SaveSettingsDialog);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        sizePolicy.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy);

        horizontalLayout_4->addWidget(label_4);

        fileNameInput = new QLineEdit(SaveSettingsDialog);
        fileNameInput->setObjectName(QString::fromUtf8("fileNameInput"));

        horizontalLayout_4->addWidget(fileNameInput);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(SaveSettingsDialog);
        label->setObjectName(QString::fromUtf8("label"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy2);

        horizontalLayout->addWidget(label);

        id_predmetuInput = new QSpinBox(SaveSettingsDialog);
        id_predmetuInput->setObjectName(QString::fromUtf8("id_predmetuInput"));
        id_predmetuInput->setMinimum(100000);
        id_predmetuInput->setMaximum(999999);
        id_predmetuInput->setValue(111111);

        horizontalLayout->addWidget(id_predmetuInput);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(SaveSettingsDialog);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(label_2);

        id_archuInput = new QSpinBox(SaveSettingsDialog);
        id_archuInput->setObjectName(QString::fromUtf8("id_archuInput"));
        id_archuInput->setMinimum(100000);
        id_archuInput->setMaximum(999999);
        id_archuInput->setValue(999999);

        horizontalLayout_2->addWidget(id_archuInput);


        verticalLayout->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label_3 = new QLabel(SaveSettingsDialog);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy);

        horizontalLayout_3->addWidget(label_3);

        id_sloupecInput = new QSpinBox(SaveSettingsDialog);
        id_sloupecInput->setObjectName(QString::fromUtf8("id_sloupecInput"));

        horizontalLayout_3->addWidget(id_sloupecInput);


        verticalLayout->addLayout(horizontalLayout_3);

        buttonBox = new QDialogButtonBox(SaveSettingsDialog);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        retranslateUi(SaveSettingsDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), SaveSettingsDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), SaveSettingsDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(SaveSettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SaveSettingsDialog)
    {
        SaveSettingsDialog->setWindowTitle(QCoreApplication::translate("SaveSettingsDialog", "Dialog", nullptr));
        label_5->setText(QCoreApplication::translate("SaveSettingsDialog", "Folder: ", nullptr));
        folderLabel->setText(QString());
        label_4->setText(QCoreApplication::translate("SaveSettingsDialog", "File name:", nullptr));
        label->setText(QCoreApplication::translate("SaveSettingsDialog", "id_predmetu:", nullptr));
        label_2->setText(QCoreApplication::translate("SaveSettingsDialog", "id_archu:", nullptr));
        label_3->setText(QCoreApplication::translate("SaveSettingsDialog", "id_sloupec:", nullptr));
    } // retranslateUi

};

namespace Ui {
    class SaveSettingsDialog: public Ui_SaveSettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SAVESETTINGSDIALOG_H
