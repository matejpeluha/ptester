/********************************************************************************
** Form generated from reading UI file 'reportdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_REPORTDIALOG_H
#define UI_REPORTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ReportDialog
{
public:
    QVBoxLayout *verticalLayout;
    QTextBrowser *textBrowser;
    QHBoxLayout *horizontalLayout;
    QPushButton *openInternetButton;

    void setupUi(QDialog *ReportDialog)
    {
        if (ReportDialog->objectName().isEmpty())
            ReportDialog->setObjectName(QString::fromUtf8("ReportDialog"));
        ReportDialog->resize(909, 680);
        ReportDialog->setStyleSheet(QString::fromUtf8("#ReportDialog {\n"
"    background-color: rgb(30, 30, 30);\n"
"}\n"
"\n"
"#textBrowser{\n"
"    border: 2px solid #8f8f91;\n"
"    border-radius: 6px;\n"
"   color: rgb(255, 255, 255);\n"
"    background-color: rgb(100, 100, 100);\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(60, 60, 60), stop: 1 rgb(30, 30, 30));\n"
"}\n"
"QPushButton {\n"
"    border: 2px solid #8f8f91;\n"
"    border-radius: 6px;\n"
"	background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 1 rgb(120, 120, 120), stop: 0 #dadbde);\n"
"	min-width: 80px;\n"
"	min-height: 20px;\n"
"	max-width: 250px;\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:hover { color: rgb(80, 80, 80);}"));
        verticalLayout = new QVBoxLayout(ReportDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        textBrowser = new QTextBrowser(ReportDialog);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));

        verticalLayout->addWidget(textBrowser);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 0);
        openInternetButton = new QPushButton(ReportDialog);
        openInternetButton->setObjectName(QString::fromUtf8("openInternetButton"));

        horizontalLayout->addWidget(openInternetButton);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(ReportDialog);

        QMetaObject::connectSlotsByName(ReportDialog);
    } // setupUi

    void retranslateUi(QDialog *ReportDialog)
    {
        ReportDialog->setWindowTitle(QCoreApplication::translate("ReportDialog", "Dialog", nullptr));
        openInternetButton->setText(QCoreApplication::translate("ReportDialog", "Open in Internet Browser", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ReportDialog: public Ui_ReportDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_REPORTDIALOG_H
