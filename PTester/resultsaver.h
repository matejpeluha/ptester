#ifndef RESULTSAVER_H
#define RESULTSAVER_H

#include <QFile>
#include <QTableWidget>
#include <QProgressBar>

/*---------------------
 trieda sluziaca na ukladanie vysledkov testovania do csv suboru
---------------------*/

class ResultSaver
{
public:
    ResultSaver(QString& homeworksFolderPath, QString& fileName);

    void saveResultsFrom(QTableWidget& homeworksTable);

    void setSubjectId(int subjectId);
    void setArchiveId(int archiveId);
    void setAisColumnId(int aisColumnId);

    void setProgressBar(QProgressBar *value);

private:
    QString folderPath; //cesta k priecinku s domacimi ulohami kde bude aj ulozeny csv subor
    QString fileName;   //nazov csv suboru
    QFile* csvFile; //csv subor
    QTextStream* csvFileStream; //stream do csv suboru

    int subjectId;  //id predmetu (potrebne pre ais)
    int archiveId;  //id archivu (potrebne pre ais)
    int aisColumnId;    //id stlpca (potrebne pre ais)

    QProgressBar* progressBar;  //progress bar ukladania udajov

    void openCsvFile();
    void setCsvFile();
    void fillStudyId();

    void fillSubjectId();
    void fillArchiveId();
    void fillAisColumnId();
    void fillEveryId();
    void closeCsvFile();
    void initProgressBar(int maximum);
};

#endif // RESULTSAVER_H
