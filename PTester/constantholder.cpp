#include "constantholder.h"

ConstantHolder::ConstantHolder()
{

}

/* ------------------------------
iba funckie vracajuce stringy
--------------------------------- */

QString ConstantHolder::getREPORT_PATH_END()
{
    return "_report.html";
}

QString ConstantHolder::getCSV_RESULTS_STRING()
{
    return "_results.csv";
}

QString ConstantHolder::getFAILED_COMPILED_TEST()
{
    return "compile: FAIL";
}

QString ConstantHolder::getPASSED_COMPILED_TEST()
{
    return "compile: PASS";
}

QString ConstantHolder::getREPORT_HTML()
{
    return "report.html";
}

QString ConstantHolder::getMAIN_EXE_PATH_END()
{
    return "/main.exe";
}

QString ConstantHolder::getMAIN_C_PATH_END()
{
    return "/main.c";
}

QString ConstantHolder::getMAIN_CPP_PATH_END()
{
    return "/main.cpp";
}

QString ConstantHolder::getTIMER_SYMBOL()
{
    return "TIMER:";
}

QString ConstantHolder::getOUTPUT_SYMBOL()
{
    return "O:";
}

QString ConstantHolder::getINPUT_SYMBOL()
{
    return "I:";
}

QString ConstantHolder::getSWITCHERS_SYMBOL()
{
    return "A:";
}

QString ConstantHolder::getTEST_END_SYMBOL()
{
    return "CLOSE#";
}

QString ConstantHolder::getSCENARIO_END_SYMBOL()
{
    return "END@";
}

QString ConstantHolder::getTEST_SYMBOL()
{
    return "#";
}

QString ConstantHolder::getSCENARIO_SYMBOL()
{
    return "@";
}

QString ConstantHolder::getSTRICT_SCENARIO_SYMBOL()
{
    return "@STRICT";
}
