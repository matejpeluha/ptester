/********************************************************************************
** Form generated from reading UI file 'ptestscenariodialog.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PTESTSCENARIODIALOG_H
#define UI_PTESTSCENARIODIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_PTestScenarioDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *scenarioNameLabel;
    QLabel *label;
    QSpinBox *timer;
    QTableWidget *testsTable;
    QHBoxLayout *horizontalLayout;
    QPushButton *addTestButton;
    QPushButton *generateOutputButton;

    void setupUi(QDialog *PTestScenarioDialog)
    {
        if (PTestScenarioDialog->objectName().isEmpty())
            PTestScenarioDialog->setObjectName(QString::fromUtf8("PTestScenarioDialog"));
        PTestScenarioDialog->resize(1576, 574);
        PTestScenarioDialog->setStyleSheet(QString::fromUtf8("QDialog {\n"
"	color: rgb(0, 0 ,0);\n"
"    background-color: rgb(70, 70, 70);\n"
"}\n"
"\n"
"QTableWidget {\n"
"border: no;\n"
"   color: rgb(255, 255, 255);\n"
"    background-color: rgb(100, 100, 100);\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(140, 140, 140), stop: 1 rgb(70, 70, 70));\n"
"}\n"
"\n"
"\n"
"\n"
"QPushButton {\n"
"    border: 2px solid #8f8f91;\n"
"    border-radius: 6px;\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 1 rgb(160, 160, 160), stop: 0 #dadbde);\n"
"	min-width: 80px;\n"
"	min-height: 30px;\n"
"	max-width: 450px;\n"
"}\n"
"\n"
"\n"
"QPushButton:pressed {\n"
"    background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 #dadbde, stop: 1 #f6f7fa);\n"
"}\n"
"\n"
"QPushButton:hover { color: rgb(80, 80, 80);}"));
        verticalLayout = new QVBoxLayout(PTestScenarioDialog);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        scenarioNameLabel = new QLabel(PTestScenarioDialog);
        scenarioNameLabel->setObjectName(QString::fromUtf8("scenarioNameLabel"));
        scenarioNameLabel->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));

        verticalLayout->addWidget(scenarioNameLabel);

        label = new QLabel(PTestScenarioDialog);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("#label\n"
"{\n"
"	color: white;\n"
"}"));

        verticalLayout->addWidget(label);

        timer = new QSpinBox(PTestScenarioDialog);
        timer->setObjectName(QString::fromUtf8("timer"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(timer->sizePolicy().hasHeightForWidth());
        timer->setSizePolicy(sizePolicy);
        timer->setSizeIncrement(QSize(0, 0));
        timer->setBaseSize(QSize(0, 0));
        timer->setReadOnly(false);
        timer->setMinimum(1);
        timer->setMaximum(40);
        timer->setValue(10);

        verticalLayout->addWidget(timer);

        testsTable = new QTableWidget(PTestScenarioDialog);
        if (testsTable->columnCount() < 6)
            testsTable->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setTextAlignment(Qt::AlignCenter);
        __qtablewidgetitem->setBackground(QColor(255, 255, 255));
        testsTable->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setTextAlignment(Qt::AlignCenter);
        testsTable->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        __qtablewidgetitem2->setTextAlignment(Qt::AlignCenter);
        testsTable->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        __qtablewidgetitem3->setTextAlignment(Qt::AlignCenter);
        testsTable->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        __qtablewidgetitem4->setTextAlignment(Qt::AlignCenter);
        testsTable->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        __qtablewidgetitem5->setTextAlignment(Qt::AlignCenter);
        testsTable->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        testsTable->setObjectName(QString::fromUtf8("testsTable"));
        testsTable->setAutoFillBackground(false);
        testsTable->setStyleSheet(QString::fromUtf8("QTableCornerButton, QTableCornerButton::section{border:no;background-color:rgb(70,70,70);}\n"
"QHeaderView, QHeaderView::section {color:white; background-color:rgb(70, 70, 70);}\n"
"QTableView::item:selected{background-color: rgba(200,200,200,150); color: black };\n"
""));
        testsTable->verticalHeader()->setVisible(false);

        verticalLayout->addWidget(testsTable);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(40);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(-1, -1, -1, 0);
        addTestButton = new QPushButton(PTestScenarioDialog);
        addTestButton->setObjectName(QString::fromUtf8("addTestButton"));
        addTestButton->setMinimumSize(QSize(84, 34));
        addTestButton->setMaximumSize(QSize(454, 16777215));
        addTestButton->setBaseSize(QSize(200, 40));

        horizontalLayout->addWidget(addTestButton);

        generateOutputButton = new QPushButton(PTestScenarioDialog);
        generateOutputButton->setObjectName(QString::fromUtf8("generateOutputButton"));
        generateOutputButton->setMinimumSize(QSize(84, 34));
        generateOutputButton->setMaximumSize(QSize(454, 16777215));
        generateOutputButton->setBaseSize(QSize(200, 40));

        horizontalLayout->addWidget(generateOutputButton);


        verticalLayout->addLayout(horizontalLayout);


        retranslateUi(PTestScenarioDialog);

        QMetaObject::connectSlotsByName(PTestScenarioDialog);
    } // setupUi

    void retranslateUi(QDialog *PTestScenarioDialog)
    {
        PTestScenarioDialog->setWindowTitle(QCoreApplication::translate("PTestScenarioDialog", "Dialog", nullptr));
        scenarioNameLabel->setText(QString());
        label->setText(QCoreApplication::translate("PTestScenarioDialog", "Maximum time (in seconds) for one test", nullptr));
        QTableWidgetItem *___qtablewidgetitem = testsTable->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("PTestScenarioDialog", "Arguments", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = testsTable->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("PTestScenarioDialog", "Inputs", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = testsTable->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("PTestScenarioDialog", "Outputs", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = testsTable->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("PTestScenarioDialog", "Points", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = testsTable->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("PTestScenarioDialog", "Time/Errors", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = testsTable->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("PTestScenarioDialog", "Remove", nullptr));
        addTestButton->setText(QCoreApplication::translate("PTestScenarioDialog", "Add New Test", nullptr));
        generateOutputButton->setText(QCoreApplication::translate("PTestScenarioDialog", "Generate Output", nullptr));
    } // retranslateUi

};

namespace Ui {
    class PTestScenarioDialog: public Ui_PTestScenarioDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PTESTSCENARIODIALOG_H
