#include "ptestscenario.h"

#include <QVector>



PTestScenario::PTestScenario()
{
    this->name = "";
    this->strict = false;
    this->maxTime = 10;
    this->numberOfTests = 0;
    this->scenarioPoints = 0;
}

PTestScenario::~PTestScenario()
{
    qDeleteAll(this->allScenarioTests);
}

bool PTestScenario::getStrict() const
{
    return this->strict;
}

void PTestScenario::setStrict(bool value)
{
    this->strict = value;
}

int PTestScenario::getMaxTime() const
{
    return this->maxTime;
}

void PTestScenario::setMaxTime(int value)
{
    this->maxTime = value;
}

int PTestScenario::getNumberOfTests() const
{
    return this->numberOfTests;
}

void PTestScenario::incrementNumberOfTests()
{
    this->numberOfTests++;
}

PTestScenarioTest* PTestScenario::getTest(int index)
{
    return this->allScenarioTests.at(index);
}

void PTestScenario::addTest(PTestScenarioTest *test)
{
    this->scenarioPoints += test->getPoints();

    this->allScenarioTests.push_back(test);
    incrementNumberOfTests();
}


const QString& PTestScenario::getName() const
{
    return this->name;
}

void PTestScenario::setName(QString& name)
{
    this->name = name;
}

double PTestScenario::getScenarioPoints(){
    return scenarioPoints;
}
