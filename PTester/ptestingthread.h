#ifndef PTESTINGPROCESS_H
#define PTESTINGPROCESS_H

#include "ptestscenario.h"

#include <QObject>
#include <QFile>
#include <QMutex>
#include <QProcess>
#include <QTableWidget>
#include <QTextStream>

/*---------------------
 trieda vykonavajuca testovanie vo vlaknach
---------------------*/

class PTestingThread : public QObject
{
    Q_OBJECT
public:
    PTestingThread(QTableWidget* testTable, int* nextTestIndex);
    ~PTestingThread();

    void loadScenarios(QVector<PTestScenario*>& allScenarios);
    void killEverything();
private:
    bool testThis;  //hovori o tom ci sa aktualna domaca uloha moze testovat
    bool testNext;  //hovori o tom ci sa dalsia domaca uloha moze testovat
    bool forcedStop;    //hovori o tom ci je zastavenie testovania vynutene (true)

    QTableWidget* testTable;    //tabulka domacich uloh na testovanie
    int* nextHomeworkIndex;     //index najblizsej volnej netestovanej domacej ulohy, zdielana info medzi vlaknami
    int actualHomeworkIndex;    //aktualne testovana domaca uloha v tomto vlakne

    QVector<PTestScenario*> allScenarios;   //vsetky testovacie scenare
    QVector<QVector<int>> waitingScenarios; //vektor obsahujuci vsetky nedokonconcene testovacie scenare ktore su reprezentovane vectorom cisel(cisla reprezentuju nevykonane testy
    int activeScenarioIndex;    //index aktualne vykonavaneho testovacieho scenar
    int activeTestIndex;    //index aktualne vykonavaneho testu v ramci jeho testovacieho scenara

    QString mainPath;   //cesta k mainu
    QString homeworkPath;   //cesta k domacej ulohe
    QStringList arguments;  //argumenty vstupujuce do domacej ulohy pri spusteni
    QString errorText;  //chybovy vystup
    QString outputText; //standardny vystup
    QFile* report;  //html report domacej ulohy
    QTextStream* reportStream;  //stream do html reportu

    int tableIndex; //index v tabulke
    double points;  //body za domacu ulohu
    double scenarioPoints;  //body za domacu ulohu za prebiehajuci testovaci scenar
    double maxPTestPoints;  //maximalny pocet bodov
    bool resultStrictScenario;  //vyjadruje ci presli vsetky testy v "strict" testovacom scenari
    bool isNewScenario; //vyjadruje ci je na rade novy testovaci scenar


    QProcess* process;  //process spustajuci domacu ulohu
    QMutex* mutex;  //mutex (takzvany zamok) je potrebny na zamknutie casti kodu so zdielanou pamatou aby viac vlakien nepristupoval k nemu naraz
    QTimer* killtimer; //casovac pre meranie casu pre jeden test na domacej ulohe


    void loadWaitingScenariosVector();
    void loadWaitingScenarioTestsVector(int numberOfScenarioTests);

    void testHomework();
    void testHomeworkMain();
    void testHomeworkMainCPP();
    void testExistingHomework();
    void compileTest();
    void tryCompile();

    void interpretCompileTest();
    bool errorOccured();
    void createCompileArgument();

    void openReport();
    void addCompilationFailedToReport();
    void addMissingToReport();
    void addCompilationPassToReport();
    void closeReport();

    void writeValidPoints();
    void writeErrorPoints();
    void writePoints(QString pointsInfo);

    void setPaths();
    void setPoints();
    void addScenarioPoints();

    void setIndex();
    void testNextFile();   

    void startTest();
    void transformAllInputs(QString &inputText);
    void handleTestResult(bool result);

    void setTestProcess();
    void executeTestProcess();

    void finishHomeworkTesting();
    void prepareForNextTest();
    void takeNextScenario();
    void transformOutput(QString &outputText);
    void initTimer(int maxTime);
    void createAllConnections();
    void clearProcessOtuputs();
    void finishQProcess();
    void writeScenarioHead(const QString& scenarioName);
    void writeTestPointsToReport(double points, double maxPoints);
    void writeScenarioPointsToHtml(double maxScenarioPoints);
    void writeOutputsToReport(QString &expectedOutput);
    void writeInputsToReport(QString &&inputs);



signals:
    void readyForNextFile();    //emituje sa ked sa dokonci vsetko testovanie pre jednu domacu ulohu, aby prisla na rad dalsia
    void readyForNextTest();    //emituje sa ked sa dokonci jeden konkretny test, aby prisiel na radu dalsi test
    void actualTestingHomework();   //emituje sa tiez ked sa dokonci uloha, aby sa v PTestingManagerovi aktualizovali informacie
    
public slots:
    void run();
    void runOne();

private slots:
    void handleCompileTest();

    void takeNextTest();

    void testProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void terminateProcess();
    void saveOutput();
    void saveError();
};

#endif // PTESTINGPROCESS_H
