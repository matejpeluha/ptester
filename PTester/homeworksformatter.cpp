#include "homeworksformatter.h"

#include <QDir>
#include <QStringList>

HomeworksFormatter::HomeworksFormatter()
{

}


//v priecinku s domacimi ulohami prejde program vsetky .c a .cpp subory ktore nie su zabalene v dalsom priecinku
//kazdy takyto subor zabali do samostatneho priecinku
void HomeworksFormatter::formatHomeworks(QString& homeworksDirectory){
    QStringList nameFilter("*.c");
    nameFilter.push_back("*.cpp");
    QDir directory(homeworksDirectory);
    QStringList files = directory.entryList(nameFilter);

    for(QString& fileName : files)
        wrapToFolder(homeworksDirectory, fileName);
}


//jeden subor zabali do vlastneho priecinku podla pomenovania suboru
void HomeworksFormatter::wrapToFolder(QString& homeworksDirectory, QString& fileName){
    QString folderName = fileName.split("_-_").first();
    QString folderPath = homeworksDirectory + "/" + folderName;

    if(QDir().exists(folderPath))
        return;

    QDir().mkdir(folderPath);

    QString oldFilePath = homeworksDirectory + "/" + fileName;

    QString mainEnd = fileName.split(".").last();
    QString newFilePath = folderPath + "/main." + mainEnd;

    QFile file(oldFilePath);
    file.rename(newFilePath);
}
