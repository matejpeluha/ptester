#include "savesettingsdialog.h"
#include "ui_savesettingsdialog.h"

#include "QDebug"

SaveSettingsDialog::SaveSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveSettingsDialog)
{
    ui->setupUi(this);

    //do atributov triedy nacitaj posledne udaje z inputov dialogu
    this->id_archu = this->ui->id_archuInput->value();
    this->id_predmetu = this->ui->id_predmetuInput->value();
    this->id_sloupec = this->ui->id_sloupecInput->value();
    this->fileName = this->RESULTS_CSV;

    setWindowTitle("Save settings");
    setWindowFlags(windowFlags() | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
}

SaveSettingsDialog::~SaveSettingsDialog()
{
    delete ui;
}

QString SaveSettingsDialog::getFolder() const
{
    return folder;
}

QString SaveSettingsDialog::getFileName() const
{
    return fileName;
}

int SaveSettingsDialog::getId_predmetu() const
{
    return id_predmetu;
}

int SaveSettingsDialog::getId_sloupec() const
{
    return id_sloupec;
}

int SaveSettingsDialog::getId_archu() const
{
    return id_archu;
}


//slot pre klik na buttonBox tlacidlo, ktorym uzivatel potvrdi nastavenia pre ukladany subor
void SaveSettingsDialog::on_buttonBox_accepted()
{
    this->id_archu = this->ui->id_archuInput->value();
    this->id_predmetu = this->ui->id_predmetuInput->value();
    this->id_sloupec = this->ui->id_sloupecInput->value();
    this->fileName = this->ui->fileNameInput->text();
    this->folder = this->ui->folderLabel->text();

    //signalom da vediet MainWindow ze je to ulozene
    emit saveInputsAccepted();
}

void SaveSettingsDialog::setFolder(const QString &folder)
{
    this->folder = folder;
}


//nastavi do inputov dialogu posledne nastavenia
void SaveSettingsDialog::setLastSettings()
{
    if(!this->fileName.isEmpty())
        this->ui->fileNameInput->setText(this->fileName);

    if(!this->folder.isEmpty())
        this->ui->folderLabel->setText(this->folder);

    this->ui->id_archuInput->setValue(this->id_archu);
    this->ui->id_predmetuInput->setValue(this->id_predmetu);
    this->ui->id_sloupecInput->setValue(this->id_sloupec);
}


//slot pre zmenu textu vo fileNameInput inpute tak aby to vzdy koncilo s csv
void SaveSettingsDialog::on_fileNameInput_textChanged()
{
    QString fileName = this->ui->fileNameInput->text();

    if(fileName.isEmpty() || fileName.startsWith("."))
        this->ui->fileNameInput->setText(this->RESULTS_CSV);

    if(!fileName.endsWith(".csv")){
        QString fileNameBase = fileName.split(".").first();

        this->ui->fileNameInput->setText(fileNameBase + ".csv");
    }
}
