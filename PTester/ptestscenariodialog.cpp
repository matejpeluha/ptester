#include "constantholder.h"
#include "ptestscenariodialog.h"
#include "ui_ptestscenariodialog.h"

#include <QFileDialog>
#include <QHBoxLayout>
#include <QTextStream>
#include <QDebug>
#include <QVector>
#include <QElapsedTimer>


PTestScenarioDialog::PTestScenarioDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PTestScenarioDialog)
{
    this->numberOfTests = 0;
    setUpUI();
}

PTestScenarioDialog::~PTestScenarioDialog()
{
    delete this->ui;

    qDeleteAll(this->allSpinBoxes);
    this->allSpinBoxes.clear();
}


//nastavenie ui
void PTestScenarioDialog::setUpUI(){
    this->ui->setupUi(this);

    setWindowTitle("PTest Creator");
    setWindowFlags(windowFlags() | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
    setSectionSizes();
}


//nastavenie velkosti a responzivity tabulky testov
void PTestScenarioDialog::setSectionSizes(){
    QHeaderView *headerView = this->ui->testsTable->horizontalHeader();

    headerView->setVisible(true);
    headerView->setFrameStyle(QFrame::Box | QFrame::Plain);
    headerView->setLineWidth(1);

    headerView->setSectionResizeMode(QHeaderView::Stretch);
    headerView->setSectionResizeMode(ConstantHolder::POINTS_COLUMN_INDEX, QHeaderView::Fixed);
    headerView->setSectionResizeMode(ConstantHolder::TIME_COLUMN_INDEX, QHeaderView::Fixed);
    headerView->setSectionResizeMode(ConstantHolder::REMOVE_COLUMN_INDEX, QHeaderView::Fixed);

}


//otvorenie dialogu s nastavenym menom scenara
void PTestScenarioDialog::openWithName(QString& scenarioName){
    this->ui->scenarioNameLabel->setText(scenarioName);
    this->open();
}


//slot pre kliknutie na addTestButton tlacidlo, ktorym sa prida novy prazdny test
void PTestScenarioDialog::on_addTestButton_clicked()
{
    addEmptyRow();

    setEmptyTableCells();
    addRemoveItem();
    addPointsBox();
}


//pridanie prazdneho riadku do tabulky testov
void PTestScenarioDialog::addEmptyRow(){
    this->numberOfTests++;
    this->ui->testsTable->setRowCount(this->numberOfTests);
}


//nastavenie prazdnych policok v poslednom riadku tabulky tak, aby sa do nich dalo pisat
void PTestScenarioDialog::setEmptyTableCells(){
    this->ui->testsTable->setItem(this->numberOfTests - 1, ConstantHolder::SWITCHERS_COLUMN_INDEX, new QTableWidgetItem());
    this->ui->testsTable->setItem(this->numberOfTests - 1, ConstantHolder::INPUT_COLUMN_INDEX, new QTableWidgetItem());
    this->ui->testsTable->setItem(this->numberOfTests - 1, ConstantHolder::OUTPUT_COLUMN_INDEX, new QTableWidgetItem());

    QTableWidgetItem* timeItem = new QTableWidgetItem();
    timeItem->setFlags(timeItem->flags() ^ Qt::ItemIsEditable);
    this->ui->testsTable->setItem(this->numberOfTests - 1, ConstantHolder::TIME_COLUMN_INDEX, timeItem);
}


//pridanie spinboxu na body do posledneho riadku tabulky testov
void PTestScenarioDialog::addPointsBox(){
    QWidget* spinBoxWidget = new QWidget();
    QDoubleSpinBox* spinBox = new QDoubleSpinBox();
    setUpSpinBox(spinBox);

    QHBoxLayout* layoutCheckBox = new QHBoxLayout(spinBoxWidget);
    layoutCheckBox->addWidget(spinBox);
    layoutCheckBox->setAlignment(Qt::AlignCenter);

    this->ui->testsTable->setCellWidget(this->numberOfTests - 1, ConstantHolder::POINTS_COLUMN_INDEX, spinBoxWidget);
    this->allSpinBoxes.push_back(spinBox);
}


//nastavenie spinboxu na body na defaultne hodnoty
void PTestScenarioDialog::setUpSpinBox(QDoubleSpinBox* spinBox){
    spinBox->setValue(0.2);
    spinBox->setDecimals(2);
    spinBox->setSingleStep(0.1);
    spinBox->setMinimumHeight(20);
}


//pridanie policka na odstranenie testu do posledneho riadku tabulky testov
void PTestScenarioDialog::addRemoveItem(){
    QIcon icon;
    QSize size(16, 16);
    icon.addPixmap(style()->standardIcon(QStyle::SP_TrashIcon).pixmap(size), QIcon::Normal);

    QTableWidgetItem* removeItem = new QTableWidgetItem();
    removeItem->setFlags(removeItem->flags() & ~Qt::ItemIsEditable);
    removeItem->setTextAlignment(Qt::AlignCenter);
    removeItem->setText("REMOVE");
    removeItem->setIcon(icon);

    this->ui->testsTable->setItem(this->numberOfTests - 1, ConstantHolder::REMOVE_COLUMN_INDEX, removeItem);
}


//slot pre dvojkliknutie na policko tabulky testov(vyuzivane len pre odstranovanie testov)
void PTestScenarioDialog::on_testsTable_cellDoubleClicked(int row, int column)
{
    if(column == ConstantHolder::REMOVE_COLUMN_INDEX)
        removeRow(row);
}


//odstranenie riadka spolu so spinboxom
void PTestScenarioDialog::removeRow(int row){
    this->ui->testsTable->removeRow(row);
    this->numberOfTests--;
    this->allSpinBoxes.remove(row);
}


//slot pre kliknutie na generateOutputButton tlacidlo vdaka ktoremu vieme urcit ktory .exe nam vygeneruje outputy
//pre vsetky testy v scenari
void PTestScenarioDialog::on_generateOutputButton_clicked()
{
    this->sampleExecutable = QFileDialog::getOpenFileName(this, "Choose executable of homework's sample", QDir::homePath(), tr("Executables (*.exe)"));

    if(this->sampleExecutable == "")
        return;

    for(int testIndex = 0; testIndex < this->numberOfTests; testIndex++)
        testSwitchersAndInput(testIndex);
}


//otestuje prepinace (vstupne argumenty programu) a inputy vybraneho testu scenara na vybranom .exe subore
void PTestScenarioDialog::testSwitchersAndInput(int testIndex){
    int maxTime = this->ui->timer->value() * 1000;

    //priprava prepinacov (vstupnych argumentov)
    QString switchersText = this->ui->testsTable->item(testIndex, ConstantHolder::SWITCHERS_COLUMN_INDEX)->text();
    QStringList switchers = switchersText.split(QRegExp("\\s+"), QString::SkipEmptyParts);

    //nastavenie qprocessu na spustenie pozadovaneho programu
    QProcess process;
    process.setProgram(this->sampleExecutable);
    process.setArguments(switchers);

    //priprava inputov pogramu
    QString inputText = this->ui->testsTable->item(testIndex, ConstantHolder::INPUT_COLUMN_INDEX)->text();
    QStringList splittedInputs = inputText.split("\\n");
    splittedInputs.removeLast();

    //spustenie .exe programu
    process.start(QIODevice::Unbuffered | QIODevice::ReadWrite);

    //posunutie inputov do programu
    for(QString& input : splittedInputs){
        process.write((input.toStdString() + "\n").c_str());
        process.waitForBytesWritten(maxTime);
    }

    //zapisanie "newline characteru" pre pripad ze program na ukoncenie potrebuje stlacit enter bez ineho inputu
    process.write("\n");

    //cakanie na dokoncenie qprocessu s casovym ohranicenim
    //netreba to robit zlozito cez connecty vzhladom na jednoduchost daneho problemu
    bool result = process.waitForFinished(maxTime);
    process.closeWriteChannel();

    //vyhodnotenie a vypisanie vysledkov do tabulky - output aj informacia o tom ci nenastala dakde chyba
    if(result){
        QString outputText = process.readAllStandardOutput();
        formatOutput(outputText);
        this->ui->testsTable->setItem(testIndex, ConstantHolder::OUTPUT_COLUMN_INDEX, new QTableWidgetItem(outputText));
        this->ui->testsTable->setItem(testIndex, ConstantHolder::TIME_COLUMN_INDEX, new QTableWidgetItem("OK"));
    }
    else{
        this->ui->testsTable->setItem(testIndex, ConstantHolder::TIME_COLUMN_INDEX, new QTableWidgetItem("FAIL"));
    }

    //zavretie processu
    process.close();

    //v priapde zamrznutia aplikacie, sa vykonaju ulohy v event loope ktore cakaju v queue na vykonanie
    QCoreApplication::processEvents();
}


//fomatovanie outputu do potrebneho tvara
void PTestScenarioDialog::formatOutput(QString& outputText){
    //v pripade prilis velkeho outputu prebehne skratenie na 1000 znakov
    int maxOutputLength = outputText.length();
    if(maxOutputLength > 1000){
        maxOutputLength = 1000;
        outputText = outputText.left(maxOutputLength);
    }

    //"newline charactery" su vymene za ich stringovu verziu, nakolko outpotu chcem zobrazit v jednom riadku a vidiet kde maju byt oddelenia riadkov
    if(outputText.contains("\r\n"))
        outputText.replace("\r\n", "\\n");
    if(outputText.contains("\n\n"))
        outputText.replace("\n\n", "\n");
    if(outputText.contains("\n"))
        outputText.replace("\n", "\\n");
}


//transformovanie inputov do potrebneho tvaru a ulozenie do vectora
void PTestScenarioDialog::transformAllInputs(QVector<const char *>& allInputs, QString& inputText){
    QStringList splittedInputs = inputText.split("\\n");
    splittedInputs.removeLast();

    for(QString& input: splittedInputs){
        input += "\n";
        std::string inputString = input.toStdString();
        const char* input_c = inputString.c_str();
        allInputs.push_back(input_c);
    }
}


//ulozenie scenara a testov do .ptest suboru
void PTestScenarioDialog::saveTestsInFile(QTextStream* pTestStream){
    this->pTestStream = pTestStream;

    *this->pTestStream << ConstantHolder::getTIMER_SYMBOL() << this->ui->timer->value() << "\n\n";

    for(int testIndex = 0; testIndex < this->numberOfTests; testIndex++){
        writeTestHeaderToFile(testIndex);
        writeTestBodyToFile(testIndex);
    }

    *this->pTestStream << ConstantHolder::getSCENARIO_END_SYMBOL() << "\n\n";
}


//zapisanie hlavicky testu do .ptest suboru
void PTestScenarioDialog::writeTestHeaderToFile(int testIndex){
    QDoubleSpinBox* spinBox = this->allSpinBoxes.at(testIndex);

    *this->pTestStream << ConstantHolder::getTEST_SYMBOL() << spinBox->value() << "\n";
}


//zapisanie tela testu do .ptest subru vratane inputov, outputov a prepinacov
void PTestScenarioDialog::writeTestBodyToFile(int testIndex){
    *this->pTestStream << ConstantHolder::getSWITCHERS_SYMBOL() << this->ui->testsTable->item(testIndex, ConstantHolder::SWITCHERS_COLUMN_INDEX)->text() << "\n";
    *this->pTestStream << ConstantHolder::getINPUT_SYMBOL() << this->ui->testsTable->item(testIndex, ConstantHolder::INPUT_COLUMN_INDEX)->text() << "\n";
    *this->pTestStream << ConstantHolder::getOUTPUT_SYMBOL() << this->ui->testsTable->item(testIndex, ConstantHolder::OUTPUT_COLUMN_INDEX)->text() << "\n";
    *this->pTestStream << ConstantHolder::getTEST_END_SYMBOL() << "\n\n";
}


//nacitanie testovacieho scenara z .ptest suboru
void PTestScenarioDialog::loadScenario(QTextStream* pTestStream)
{
    this->pTestStream = pTestStream;

    while(!this->pTestStream->atEnd()){
        QString line = this->pTestStream->readLine();
        if(line.startsWith(ConstantHolder::getSCENARIO_END_SYMBOL()))
            break;

        else if(line.startsWith(ConstantHolder::getTIMER_SYMBOL()))
            loadScenarioTimer(line);

        else if(line.startsWith(ConstantHolder::getTEST_SYMBOL()))
            loadTest(line);
    }
}


//nacitanie timera pre testovaci scenar z .ptest suboru
void PTestScenarioDialog::loadScenarioTimer(QString& line){
    int maxTime = line.section(ConstantHolder::getTIMER_SYMBOL(), 1, 1).toInt();
    this->ui->timer->setValue(maxTime);
}


//nacitanie jedneho testu z .ptest suboru, prida prazdny riadok do tabulky testov a zapise test
void PTestScenarioDialog::loadTest(QString& line){
    on_addTestButton_clicked();

    double points = line.section(ConstantHolder::getTEST_SYMBOL(), 1, 1).toDouble();
    this->allSpinBoxes.last()->setValue(points);

    loadTestAttributes();
}

//zapise do testu v tabulke testov vsetky atributy potrebne pre test
void PTestScenarioDialog::loadTestAttributes(){
    QString line;
    bool switchersApproached = false;
    bool inputApproached = false;
    bool outputApproached = false;

    while(!this->pTestStream->atEnd()){
        line = this->pTestStream->readLine();
        if(line.startsWith(ConstantHolder::getSCENARIO_END_SYMBOL()) || line.startsWith(ConstantHolder::getTEST_END_SYMBOL()))
            return;

        else if(line.startsWith(ConstantHolder::getSWITCHERS_SYMBOL())){
            writeInfoToTest(line, ConstantHolder::SWITCHERS_COLUMN_INDEX);
            switchersApproached = true;
        }

        else if(line.startsWith(ConstantHolder::getINPUT_SYMBOL())){
            writeInfoToTest(line, ConstantHolder::INPUT_COLUMN_INDEX);
            inputApproached = true;
        }

        else if(line.startsWith(ConstantHolder::getOUTPUT_SYMBOL())){
            writeInfoToTest(line, ConstantHolder::OUTPUT_COLUMN_INDEX);
            outputApproached = true;
        }

        else if(switchersApproached && inputApproached && outputApproached)
            return;
    }
}


//zapisae konkretnu informaciu(stlpec tabulky testov) do testu (riadok v tabulke testov)
void PTestScenarioDialog::writeInfoToTest(QString& line, int column){
    int row = this->numberOfTests - 1;
    line.remove(0, 2);
    QTableWidgetItem* item = new QTableWidgetItem(line);
    this->ui->testsTable->setItem(row, column, item);
}


//zisti maximalny cas ktory sa mzoe vykonavat jeden test
int PTestScenarioDialog::getMaxTime(){
    return this->ui->timer->value();
}


//slot ktory zaistuje ze vzdy ked sa zmeni text v policku pre inputy alebo outputy v tabulke testov
//tak vzdy skontroluje ci je zapisany "newline character" v stringovej podobe na konci
//a ked nie tak ho doplni
//je to pomocka pre uzivatela ktory moze zabudat tento znak dopisat na koniec
void PTestScenarioDialog::on_testsTable_cellChanged(int row, int column)
{
    if(column != ConstantHolder::INPUT_COLUMN_INDEX && column != ConstantHolder::OUTPUT_COLUMN_INDEX)
        return;

    QTableWidgetItem* item = this->ui->testsTable->item(row, column);
    QString itemText = item->text();

    if(!itemText.endsWith("\\n"))
        item->setText(itemText + "\\n");

}
