#include "ptestcreatorwindow.h"
#include "ui_ptestcreatorwindow.h"
#include "startwindow.h"
#include "constantholder.h"

#include <QCloseEvent>
#include <QMessageBox>
#include <QFileDialog>
#include <QProcess>
#include <QDebug>
#include <QHBoxLayout>

PTestCreatorWindow::PTestCreatorWindow(QWidget *parent, const QString& pTestPath) : QMainWindow(parent), ui(new Ui::PTestCreatorWindow)
{
    this->pTestPath = pTestPath;
    this->numberOfScenarios = 0;
    this->lastScenarioNumber = 0;

    setUpUI();
}

PTestCreatorWindow::~PTestCreatorWindow()
{
    delete this->ui;
    delete this->pTestStream;

    qDeleteAll(this->allCheckBoxes);
    qDeleteAll(this->allScenarios);

    this->allCheckBoxes.clear();
    this->allScenarios.clear();
}

//nastavenie UI pre triedu
void PTestCreatorWindow::setUpUI(){
    this->ui->setupUi(this);
    this->ui->pTestPathLabel->setText(this->pTestPath);

    setWindowTitle("PTest Creator");
    setSectionSizes();
    loadPTestToTable();
}


//nastavenie velkosti tabulky
void PTestCreatorWindow::setSectionSizes(){
    QHeaderView *headerView = this->ui->scenariosTable->horizontalHeader();

    headerView->setVisible(true);
    headerView->setFrameStyle(QFrame::Box | QFrame::Plain);
    headerView->setLineWidth(1);

    headerView->setSectionResizeMode(QHeaderView::Stretch);
    headerView->setSectionResizeMode(ConstantHolder::REMOVE_SCENARIO_COLUMN_INDEX, QHeaderView::Fixed);

}


//nacitanie .ptest suboru do tabulky
void PTestCreatorWindow::loadPTestToTable(){
    QFileInfo pTestPathInfo(this->pTestPath);

    if(pTestPathInfo.exists() && pTestPathInfo.isFile())
        loadAllScenarios();
}


//nacitanie vsetkych scenarov
void PTestCreatorWindow::loadAllScenarios(){
    QFile pTest(this->pTestPath);
    pTest.open(QIODevice::ReadOnly);

    this->pTestStream = new QTextStream(&pTest);

    while(!this->pTestStream->atEnd()) {
        QString line = this->pTestStream->readLine();
        loadPTestScenario(line);
    }

    pTest.close();
}


//nacitanie testovacieho scenaru
void PTestCreatorWindow::loadPTestScenario(QString& line){
    if(line.startsWith(ConstantHolder::getSCENARIO_SYMBOL())){
        createScenario(line);
        this->allScenarios.last()->loadScenario(this->pTestStream);
    }
}


//vytvorenie scenara v tabulke na zaklade riadku z .ptest suboru
void PTestCreatorWindow::createScenario(QString& line){
    on_addScenarioButton_clicked();

    QString scenarioName = line.section(" ", 1, -1);
    this->ui->scenariosTable->item(this->numberOfScenarios - 1, ConstantHolder::SCENARIO_NAME_COLUMN_INDEX)->setText(scenarioName);

    if(line.startsWith(ConstantHolder::getSTRICT_SCENARIO_SYMBOL()))
        this->allCheckBoxes.last()->setChecked(true);
}


//slot pre kliknutie na tlacidlo addScenarioButton ktorym sa vytvori novy prazdny testovaci scenar
void PTestCreatorWindow::on_addScenarioButton_clicked()
{
    this->numberOfScenarios++;
    this->ui->scenariosTable->setRowCount(this->numberOfScenarios);
    this->allScenarios.push_back(new PTestScenarioDialog(this));

    addScenarioItem();
    addTestsItem();
    addStrictCheckBoxItem();
    addRemoveItem();
}


//pridanie mena testovacieho scenara na posledny scenar v tabulke
void PTestCreatorWindow::addScenarioItem(){
    this->lastScenarioNumber++;
    QString text = "SCENARIO_" + QString::number(this->lastScenarioNumber);
    QTableWidgetItem* item = new QTableWidgetItem(text);
    item->setTextAlignment(Qt::AlignCenter);

    this->ui->scenariosTable->setItem(this->numberOfScenarios - 1, ConstantHolder::SCENARIO_NAME_COLUMN_INDEX, item);
}


//pridanie policka pre testy na posledny scenar v tabulke
void PTestCreatorWindow::addTestsItem(){
    QString text = "TESTS";
    QTableWidgetItem* item = new QTableWidgetItem(text);
    item->setTextAlignment(Qt::AlignCenter);
    item->setFlags(item->flags() & ~Qt::ItemIsEditable);

    QIcon icon;
    QSize size(16, 16);
    icon.addPixmap(style()->standardIcon(QStyle::SP_DialogOpenButton).pixmap(size), QIcon::Normal);
    item->setIcon(icon);

    this->ui->scenariosTable->setItem(this->numberOfScenarios - 1, ConstantHolder::TESTS_COLUMN_INDEX, item);
}


//pridanie zaskrtavacie policka checkBox posledny scenar v tabulke
void PTestCreatorWindow::addStrictCheckBoxItem(){
    QWidget* checkBoxWidget = new QWidget();
    QCheckBox* checkBox = new QCheckBox();

    QHBoxLayout* layoutCheckBox = new QHBoxLayout(checkBoxWidget);
    layoutCheckBox->addWidget(checkBox);
    layoutCheckBox->setAlignment(Qt::AlignCenter);

    this->ui->scenariosTable->setCellWidget(this->numberOfScenarios - 1, ConstantHolder::STRICT_COLUMN_INDEX, checkBoxWidget);
    this->allCheckBoxes.push_back(checkBox);
}


//pridanie policka pre zmazanie testovacieho scenaru na posledny scenar v tabulke
void PTestCreatorWindow::addRemoveItem(){
    QIcon icon;
    QSize size(16, 16);
    icon.addPixmap(style()->standardIcon(QStyle::SP_TrashIcon).pixmap(size), QIcon::Normal);

    QTableWidgetItem* removeItem = new QTableWidgetItem();
    removeItem->setFlags(removeItem->flags() & ~Qt::ItemIsEditable);
    removeItem->setTextAlignment(Qt::AlignCenter);
    removeItem->setText("REMOVE");
    removeItem->setIcon(icon);

    this->ui->scenariosTable->setItem(this->numberOfScenarios - 1, ConstantHolder::REMOVE_SCENARIO_COLUMN_INDEX, removeItem);
}


//slot pre kliknutie na saveAllTestsButton tlacidlo ktorym ulozime vsetky testy v .ptest subore
void PTestCreatorWindow::on_saveAllTestsButton_clicked()
{
    QFile pTest(this->pTestPath);
    pTest.open(QIODevice::WriteOnly);

    saveScenariosInFile(pTest);

    pTest.close();

    QMessageBox::question(this, "PTest Creator", tr("SAVED!"), QMessageBox::Ok);
}


//prejde cez vsetky testovacie scenare a ulozi do .ptest suboru
void PTestCreatorWindow::saveScenariosInFile(QFile& pTest){
    for(int scenarioIndex = 0; scenarioIndex < this->numberOfScenarios; scenarioIndex++)
        saveOneScenarioInFile(scenarioIndex, pTest);
}


//ulozi konkretny scenar v .ptest subore
void PTestCreatorWindow::saveOneScenarioInFile(int scenarioIndex, QFile& pTest){
    QTextStream pTestStream(&pTest);

    writeScenarioHeaderToFile(scenarioIndex, pTestStream);
    writeTestsToFile(scenarioIndex, pTestStream);
}


//v .ptest subore zapise hlavicku konkretneho testovacieho scenara
void PTestCreatorWindow::writeScenarioHeaderToFile(int scenarioIndex, QTextStream& pTest){
    QTableWidget* scenariosTable = this->ui->scenariosTable;
    QCheckBox* checkBox = this->allCheckBoxes.at(scenarioIndex);


    if(checkBox->isChecked())
        pTest << ConstantHolder::getSTRICT_SCENARIO_SYMBOL();
    else
        pTest << ConstantHolder::getSCENARIO_SYMBOL();

    pTest << " " << scenariosTable->item(scenarioIndex, ConstantHolder::SCENARIO_NAME_COLUMN_INDEX)->text() << "\n";
}


//v .ptest subore zapise vsetky test konkretneho testovacieho scenara
void PTestCreatorWindow::writeTestsToFile(int scenarioIndex, QTextStream& pTest){
    PTestScenarioDialog* scenario = this->allScenarios.at(scenarioIndex);
    scenario->saveTestsInFile(&pTest);
}


//slot pre dvojkliknutie na policko v tabulke
void PTestCreatorWindow::on_scenariosTable_cellDoubleClicked(int row, int column)
{
    if(column == ConstantHolder::REMOVE_SCENARIO_COLUMN_INDEX)
        removeRow(row);
    else if(column == ConstantHolder::TESTS_COLUMN_INDEX)
        openTestScenarioDialog(row);
}


//odstrani riadok v tabulke testovacich scenarov
void PTestCreatorWindow::removeRow(int row){
    this->ui->scenariosTable->removeRow(row);
    this->numberOfScenarios--;
    this->allScenarios.remove(row);
    this->allCheckBoxes.remove(row);
}


//otvori dialog so vsetkymi testami pre konkretny testovaci scenar
void PTestCreatorWindow::openTestScenarioDialog(int row){
    QString scenarioName = this->ui->scenariosTable->item(row, ConstantHolder::SCENARIO_NAME_COLUMN_INDEX)->text();
    this->allScenarios.at(row)->openWithName(scenarioName);
}


//override-nuta funkcia na zatvorenie okna
//vy-emituje signal closed ak uzivatel suhlasi so zavretim okna
void PTestCreatorWindow::closeEvent (QCloseEvent *event)
{
    QMessageBox::StandardButton resBtn = QMessageBox::question( this, "PTest Creator",
                                                                tr("Are you sure ?"),
                                                               QMessageBox::No | QMessageBox::Yes);
    if (resBtn == QMessageBox::No) {
        event->ignore();
    } else {
        emit closed();
        event->accept();
    }
}

