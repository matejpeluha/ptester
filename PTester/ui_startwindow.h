/********************************************************************************
** Form generated from reading UI file 'startwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STARTWINDOW_H
#define UI_STARTWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_StartWindow
{
public:
    QGridLayout *gridLayout;
    QSpacerItem *horizontalSpacer;
    QLabel *entryLabel;
    QSpacerItem *verticalSpacer_2;
    QPushButton *newLoaderButton;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *pTestCreatorButton;
    QPushButton *pTestCreatorLoadButton;
    QPushButton *checkedLoaderButton;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *StartWindow)
    {
        if (StartWindow->objectName().isEmpty())
            StartWindow->setObjectName(QString::fromUtf8("StartWindow"));
        StartWindow->resize(958, 617);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(StartWindow->sizePolicy().hasHeightForWidth());
        StartWindow->setSizePolicy(sizePolicy);
        StartWindow->setStyleSheet(QString::fromUtf8("#StartWindow {\n"
"    background-color: rgb(30, 30, 30);\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 1 rgb(100, 100, 100), stop: 0 rgb(30, 30, 30));\n"
"}\n"
"\n"
"QPushButton {\n"
"    border: 2px solid #8f8f91;\n"
"    border-radius: 6px;\n"
"	background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 1 rgb(170, 170, 170), stop: 0 rgb(100, 100, 100));\n"
"	min-width: 80px;\n"
"}\n"
"\n"
"QPushButton:hover {	background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 1 rgb(255, 255, 255), stop: 0 rgb(120, 120, 120));\n"
"}\n"
"\n"
"QPushButton:pressed {\n"
"   background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(200, 200, 200), stop: 1 rgb(120, 120, 120));\n"
"}"));
        gridLayout = new QGridLayout(StartWindow);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 6, 0, 1, 1);

        entryLabel = new QLabel(StartWindow);
        entryLabel->setObjectName(QString::fromUtf8("entryLabel"));
        sizePolicy.setHeightForWidth(entryLabel->sizePolicy().hasHeightForWidth());
        entryLabel->setSizePolicy(sizePolicy);
        entryLabel->setMaximumSize(QSize(400, 120));
        entryLabel->setBaseSize(QSize(400, 150));
        QFont font;
        font.setPointSize(62);
        entryLabel->setFont(font);
        entryLabel->setStyleSheet(QString::fromUtf8("#entryLabel {\n"
"\n"
"	color: rgb(255, 255 ,255)\n"
"}"));

        gridLayout->addWidget(entryLabel, 2, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Preferred);

        gridLayout->addItem(verticalSpacer_2, 3, 1, 1, 1);

        newLoaderButton = new QPushButton(StartWindow);
        newLoaderButton->setObjectName(QString::fromUtf8("newLoaderButton"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(newLoaderButton->sizePolicy().hasHeightForWidth());
        newLoaderButton->setSizePolicy(sizePolicy1);
        newLoaderButton->setMinimumSize(QSize(84, 80));
        QFont font1;
        font1.setPointSize(12);
        newLoaderButton->setFont(font1);
        newLoaderButton->setStyleSheet(QString::fromUtf8(""));

        gridLayout->addWidget(newLoaderButton, 4, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 6, 2, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, -1, -1, -1);
        pTestCreatorButton = new QPushButton(StartWindow);
        pTestCreatorButton->setObjectName(QString::fromUtf8("pTestCreatorButton"));
        sizePolicy1.setHeightForWidth(pTestCreatorButton->sizePolicy().hasHeightForWidth());
        pTestCreatorButton->setSizePolicy(sizePolicy1);
        pTestCreatorButton->setMinimumSize(QSize(84, 20));
        pTestCreatorButton->setMaximumSize(QSize(200, 16777215));
        pTestCreatorButton->setFont(font1);
        pTestCreatorButton->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(pTestCreatorButton);

        pTestCreatorLoadButton = new QPushButton(StartWindow);
        pTestCreatorLoadButton->setObjectName(QString::fromUtf8("pTestCreatorLoadButton"));
        sizePolicy1.setHeightForWidth(pTestCreatorLoadButton->sizePolicy().hasHeightForWidth());
        pTestCreatorLoadButton->setSizePolicy(sizePolicy1);
        pTestCreatorLoadButton->setMinimumSize(QSize(84, 20));
        pTestCreatorLoadButton->setMaximumSize(QSize(200, 16777215));
        pTestCreatorLoadButton->setFont(font1);
        pTestCreatorLoadButton->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(pTestCreatorLoadButton);


        gridLayout->addLayout(horizontalLayout, 8, 1, 1, 1);

        checkedLoaderButton = new QPushButton(StartWindow);
        checkedLoaderButton->setObjectName(QString::fromUtf8("checkedLoaderButton"));
        sizePolicy1.setHeightForWidth(checkedLoaderButton->sizePolicy().hasHeightForWidth());
        checkedLoaderButton->setSizePolicy(sizePolicy1);
        checkedLoaderButton->setMinimumSize(QSize(84, 80));
        checkedLoaderButton->setFont(font1);

        gridLayout->addWidget(checkedLoaderButton, 6, 1, 1, 1);

        verticalSpacer = new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Preferred);

        gridLayout->addItem(verticalSpacer, 7, 1, 1, 1);


        retranslateUi(StartWindow);

        QMetaObject::connectSlotsByName(StartWindow);
    } // setupUi

    void retranslateUi(QDialog *StartWindow)
    {
        StartWindow->setWindowTitle(QCoreApplication::translate("StartWindow", "Dialog", nullptr));
        entryLabel->setText(QCoreApplication::translate("StartWindow", " PTester", nullptr));
        newLoaderButton->setText(QCoreApplication::translate("StartWindow", "Load Folder with New Homeworks", nullptr));
        pTestCreatorButton->setText(QCoreApplication::translate("StartWindow", "PTest Creator", nullptr));
        pTestCreatorLoadButton->setText(QCoreApplication::translate("StartWindow", "PTest Loader", nullptr));
        checkedLoaderButton->setText(QCoreApplication::translate("StartWindow", "Load Tested Homeworks from CSV", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StartWindow: public Ui_StartWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STARTWINDOW_H
