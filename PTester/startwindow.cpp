#include "homeworksformatter.h"
#include "startwindow.h"
#include "ui_startwindow.h"

#include <QFileDialog>
#include <QMessageBox>
#include <QtCore>
#include <QInputDialog>



StartWindow::StartWindow(QWidget *parent) : QDialog(parent), ui(new Ui::StartWindow)
{
    ui->setupUi(this);

    setWindowTitle("PTester");

    setWindowFlags(windowFlags() | Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
}

StartWindow::~StartWindow()
{
    delete this->ui;
}


//slot pre kliknutie na newLoaderButton tlacidlo ktorym si urcime priecinok s domacimi ulohami ktore chceme nacitatat a testovat
void StartWindow::on_newLoaderButton_clicked(){
    QString homeworksFolder = QFileDialog::getExistingDirectory(this, "Choose Folder", QDir::homePath(), QFileDialog::ShowDirsOnly);

    if(homeworksFolder == "")
        return;

    //naformatuje domace ulohy v podobe ze vsetky samostatne .c a .cpp splnajuce ais format zabali do vlastnych priecinkov
    HomeworksFormatter homeworksFormatter;
    homeworksFormatter.formatHomeworks(homeworksFolder);

    showMainWindow(homeworksFolder);
}


//slot pre kliknutie na checkedLoaderButton tlacidlo ktorym nacitame s vybranym csv suborom(obsahujuci ulozene vysledky testovania)
//vsetky domace ulohy uz s pridelenymi bodmi
//csv subor musi byt v priecinku s ulohami
void StartWindow::on_checkedLoaderButton_clicked(){
    const QString csvFilePath = QFileDialog::getOpenFileName(this, "Choose csv file with results", QDir::homePath(), tr("CSV(*.csv)"));

    if(csvFilePath == "")
        return;

    int directoryBashIndex = csvFilePath.lastIndexOf("/") ;
    QString homeworksFolderPath = csvFilePath.left(directoryBashIndex);

    showMainWindow(homeworksFolderPath, csvFilePath);
}


//otvori hlavne okno s nacitanymi domacimi ulohami kde ich vieme testovat
//StartWindow sa skryje a po zavreti MainWindow sa zas otvori
void StartWindow::showMainWindow(QString& homeworksFolderPath, const QString& csvFilePath){
    this->mainWindow = new MainWindow(nullptr, &homeworksFolderPath);
    connect(this->mainWindow, SIGNAL(closed(void)), this, SLOT(closeMainWindowSlot(void)));

    this->mainWindow->loadPointsFromCsv(csvFilePath);

    this->mainWindow->show();
    this->hide();
}


//slot ktory sa vola po emitovani signalu closed z MainWindow
void StartWindow::closeMainWindowSlot(){
    delete this->mainWindow;
    this->show();
}


//opytanie sa uzivatela otazku
bool StartWindow::qmessageAskYesNo(const QString& question){
    QMessageBox::StandardButton reply =
            QMessageBox::question(this, "PTester", question,
                                  QMessageBox::Yes | QMessageBox::No);

    if(reply == QMessageBox::No)
        return false;

    return true;
}


//slot pre kliknutie na pTestCreatorButton tlacidlo ktorym vieme VYTVORIT NOVY .ptest subor obsahujuci testovacie scenare s testami
void StartWindow::on_pTestCreatorButton_clicked(){
    QString pTestName = QInputDialog::getText(this, "PTest Creator", "Name your new test");
    if(pTestName == "")
        return;

    QString homeworksFolder = QFileDialog::getExistingDirectory(this, "Choose Directory for PTest", QDir::homePath(), QFileDialog::ShowDirsOnly);
    if(homeworksFolder == "")
        return;

    QString pTestPath = homeworksFolder + "/" + pTestName + ".ptest";
    startPTestCreator(pTestPath);
}


//slot pre kliknutie na pTestCreatorButton tlacidlo ktorym vieme UPRAVOVAT EXISTUJUCY .ptest subor obsahujuci testovacie scenare s testami
void StartWindow::on_pTestCreatorLoadButton_clicked()
{
    QString pTestPath = QFileDialog::getOpenFileName(this, "Choose .ptest file with tests", QDir::homePath(), tr("ptest (*.ptest)"));
    if(pTestPath == "")
        return;

    startPTestCreator(pTestPath);
}


//spustenie PTestCreator
//po jeho zavreti otvorit zase StarWindow
void StartWindow::startPTestCreator(QString& pTestPath){
    this->pTesterCreatorWindow = new PTestCreatorWindow(nullptr, pTestPath);
    connect(this->pTesterCreatorWindow, SIGNAL(closed(void)), this, SLOT(closePTestCreatorSlot(void)));

    this->pTesterCreatorWindow->show();
    this->hide();
}


//slot ktory sa vola po emitovani signalu closed od PTestCreator pri jeho zavreti
void StartWindow::closePTestCreatorSlot(){
    delete this->pTesterCreatorWindow;
    this->show();
}
