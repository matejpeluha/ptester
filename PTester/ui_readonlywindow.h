/********************************************************************************
** Form generated from reading UI file 'readonlywindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_READONLYWINDOW_H
#define UI_READONLYWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ReadOnlyWindow
{
public:
    QWidget *centralwidget;
    QHBoxLayout *horizontalLayout;
    QTableView *homeWorksTable;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *ReadOnlyWindow)
    {
        if (ReadOnlyWindow->objectName().isEmpty())
            ReadOnlyWindow->setObjectName(QString::fromUtf8("ReadOnlyWindow"));
        ReadOnlyWindow->resize(1202, 585);
        ReadOnlyWindow->setStyleSheet(QString::fromUtf8("#ReadOnlyWindow {\n"
"	color: rgb(0, 0 ,0);\n"
"    background-color: rgb(30, 30, 30);\n"
"}\n"
"\n"
"#homeWorksTable {\n"
"border: no;\n"
"   color: rgb(255, 255, 255);\n"
"    background-color: rgb(100, 100, 100);\n"
"background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,\n"
"                                      stop: 0 rgb(60, 60, 60), stop: 1 rgb(30, 30, 30));\n"
"}"));
        centralwidget = new QWidget(ReadOnlyWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        horizontalLayout = new QHBoxLayout(centralwidget);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        homeWorksTable = new QTableView(centralwidget);
        homeWorksTable->setObjectName(QString::fromUtf8("homeWorksTable"));
        homeWorksTable->setEnabled(true);
        homeWorksTable->setMouseTracking(false);
        homeWorksTable->setStyleSheet(QString::fromUtf8("QTableCornerButton, QTableCornerButton::section{border:no;background-color:rgb(30,30,30);}\n"
"QHeaderView, QHeaderView::section {color:white; background-color:rgb(30, 30, 30);}\n"
"QTableView::item:selected{background-color: rgba(200,200,200,150); color: black };\n"
""));
        homeWorksTable->setAlternatingRowColors(false);
        homeWorksTable->setSelectionBehavior(QAbstractItemView::SelectRows);
        homeWorksTable->setShowGrid(true);
        homeWorksTable->setGridStyle(Qt::SolidLine);
        homeWorksTable->setSortingEnabled(true);
        homeWorksTable->setCornerButtonEnabled(false);

        horizontalLayout->addWidget(homeWorksTable);

        ReadOnlyWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(ReadOnlyWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1202, 26));
        ReadOnlyWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(ReadOnlyWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        ReadOnlyWindow->setStatusBar(statusbar);

        retranslateUi(ReadOnlyWindow);

        QMetaObject::connectSlotsByName(ReadOnlyWindow);
    } // setupUi

    void retranslateUi(QMainWindow *ReadOnlyWindow)
    {
        ReadOnlyWindow->setWindowTitle(QCoreApplication::translate("ReadOnlyWindow", "MainWindow", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ReadOnlyWindow: public Ui_ReadOnlyWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_READONLYWINDOW_H
