#ifndef SAVEINPUTSDIALOG_H
#define SAVEINPUTSDIALOG_H

#include <QDialog>

namespace Ui {
class SaveSettingsDialog;
}

/*---------------------
 trieda pre okno(dialog) v ktorom uzivatel pred ulozenim vysledkov testovania domacich uloh, nastavi potrebne udaje
---------------------*/

class SaveSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SaveSettingsDialog(QWidget *parent = nullptr);
    ~SaveSettingsDialog();

    QString getFolder() const;

    QString getFileName() const;

    int getId_predmetu() const;

    int getId_sloupec() const;

    int getId_archu() const;

    void setFolder(const QString &value);

    void setLastSettings();
private slots:
    void on_buttonBox_accepted();

    void on_fileNameInput_textChanged();

private:
    Ui::SaveSettingsDialog *ui;

    QString folder; //subor v ktorom sa ulozi csv subor (defaultne v subore s ulohami)
    QString fileName;   //nazov csv suboru

    //udaje potrebne pre ais
    int id_predmetu;
    int id_sloupec;
    int id_archu;

    const QString RESULTS_CSV = "results.csv";

signals:
    void saveInputsAccepted();  //emituje sa ked uzivatel potvrdi nastavenia


};

#endif // SAVEINPUTSDIALOG_H
