#ifndef REPORTDIALOG_H
#define REPORTDIALOG_H

#include <QDialog>

namespace Ui {
class ReportDialog;
}

/*---------------------
 trieda pre okno(dialog) zobrazujuca html report priamo v aplikacii
---------------------*/

class ReportDialog : public QDialog
{
    Q_OBJECT

public:
    QString reportPath;

    explicit ReportDialog(QWidget *parent = nullptr);
    ~ReportDialog();

    void show(const QString& reportPath);

private slots:
    void on_openInternetButton_clicked();

private:
    Ui::ReportDialog *ui;

    void sendHtmlToTextBrowser();
};

#endif // REPORTDIALOG_H
